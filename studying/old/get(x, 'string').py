# Программа выводит словарь вложенного в словарь в одной строке.

allGuests = {'Alice': {'apples': 'tort', 'potato': 12},
             'Bob': {'carrot': 3, 'apples': 'pork'},
             }
total_brought = ''


for each in allGuests.values():
    print(each)
    for k, v in each.items():
        total_brought += k + ' ' + str(v) + '\n'
print(total_brought)
