import pprint
import re
import string

dictionary = '''
words | translate 
-- | -- |
**A**|
anymore | больше (не), уже (не)
a lot of | много
a long on | долгий, длинный
advanced | передовой, продвинутый
Advice | совет
advertisement | рекламное объявление
abilities | способности
across | через, сквозь
acceptance | принятие
available | имеется в наличии, доступен
against | против, от
Agree | согласиться
arrays | массивы
alert | тревога
alerted | предупредил
allows | позволяет, разрешено
allowed |разрешено
almost | почти
amiss |  неправильно, неладно, дурно
anchor | якорь
afford | доступный, предоставить
applicable | применимы(й)
appalled | потрясен
append | добавлять, присоединять
as | так же, так как, как 
assume | предполагать (под свою ответственность)
assignment | назначение, присвоение
Arise | возникать, появляться
at least | по меньшей (крайней) мере 
Away | прочь
aware | осведомленный
Awful | ужасный
**B**| 
bar | стойка, планка
barely | едва
betrayed | предательство
below | ниже
Besides | помимо, кроме того.
bunch | связка, группа
batch | партия, группа, пакет
built in | встроенных
blame | вина, ответственность, порицание
blast |  взрыв, звук
bottom-up | восходящая, вверх дном, беспорядочная
Boundary | граница (areas)
Border | граница (country)
boil | нарыв, кипятить, варить
brackets | скобки
breath | дыхание, вдох
broad | широкий, масштабный
binary form | в двоичной форме
being | v3, быть, являться в данный момент
**C**|
cab | такси
Cape | мыс, плащ, накидка
Carry | нести, переносить 
callable | вызываемый
calm | спокойный
Cause | причина, дело, вызвать
complicated | сложный, запутанный
custom | настраеваемые
cursed | проклятый, отвратительный
cure | лекарство, лечить
column | столбец, колонка
compile | собирать, составлять
completely | полностью, совершенно
Complain | жаловаться
compassion | сострадание 
comma | запятая
common | общий
cheat | обман, мошенничать
concern | беспокойство, концерн.
contemptible | презренный
comeuppance | возмездие
certain | определенный,  уверенный
considerable | значительный, существенный
Conveyance | перевозка
count | считать, подсчёт
coincidence | совпадение
cross-legged | сидящий, скрестив ноги
chapter | глава
chase | погоня, преследовать
clause | оговорка, предложение, пункт
**D**|
developer | разработчик, разработка
declarations | декларация, объявление
describe | описывать
degree | степень (диплом)
Degrees | градусы
Depot | склад, хранилище, станция
develop | разработать, развивать
Difficult | трудные, затруднительные
deserve | заслуживать
decipher | расшифровать, разбирать
Decide | решить, принимать решение
domain | область, сфера
doubt | сомневаться 
data types | типы данных
during | в течении, во время
define | определять
drop | уронить, падать,  капля
dignity | достоинство, титул, звание
distract | отвлечь, отвлекать
District | район
**E**|
Earn | зарабатывать, получать
ease | легкость, простота, удобство
Edge | край
earthquake | землетрясение
eventually| в конце концов
extension | расширение, распространение
extensible | расширяемый
external | внешний
even | даже 
Events | события
ever wonder | когда нибудь задумывался
elbow | локти, проталкиваться, толкать локтями
explain | объяснять
expect | ожидать, рассчитывать
exceptions | исключения
expressions | выражений
examples | примеры
to examine | исследовать, рассмотривать
essential | необходимый, важный, основной
estate | Имущество
either | или
enough | достаточно
encourage | поощрять, призывать
enjoyed | наслаждаться
excited | взолнованы, возбуждающий в восторге
expensive | дорогой, дорогостоящий
**F**|
fares | тарифы
fired | уволен
Fit | подходит, соответствует
folks | близкие (друзья, родственники)
Fog | туман, мгла
fortunate | удачливый
Forward | вперёд, пересылать, нападающий
fraud | мошенничество, обман
flexible | гибкий, свободный 
Flow | поток, течь, течение
flush | смываться, прилив, румянец
Fruition | Плоды, осуществление, наслаждение
features | особенности, функции
festering | гнойный
few | немного, мало
Feet | ноги, стопы, лапы
**G**|
glad | радостный, довольный 
guess | предполагал, угадать
guardian | опекун, попечитель
GUI application | специализированное приложение с графическим интерфейсом 
gee | ну и дела
gently | нежно, мягко
**H**|
hall | зал, коридор, холл
hazardous | опасный, рискованный
handy | удобный
hangover | похмелье
heat | жара, нагревать
heighten | повышать
heavy | тяжелый
hook(ed) | зацепка, крюк (зацепитесь)
high score | рекорд 
**I**|
implement | реализовывать, осуществлять
impede | препятсвовать, затруднять, мешать
Improve | улучшать, усовершенствовать
interactively | интерактивно
intolerable | невыносимый, нестерпимый
Increase | увеличение (размер)
Indefinite | неопределенный
indentation | оступ, углубление
independent | независимый
influence | влияние, воздействовать
injuries | травмы
insert | вставять, внести дополнения
insane | безумный, душевнобольной
instead | вместо
Insist | настаивать
insight | интуиция, понимание
issues  | вопросы
instantly | немедленно
inevitably | неизбежно
is done | сделано
item | элемент, пункт
**J** |
jolly | весёлый, добродушный
**K**|
kind | добрый, вид, тип
kind of | вроде как, как будто
**L**|
loose | свободный, болтающийся, развязывать
look sharp | смотри в оба 
Look after | посмотреть, приглядывать
linking | связывание, соединение
Little | мало, немного
Least | наименее, меньше всего
Let | позволять, дать
letter | письмо, буква
**M**|
Mark | отметка, маркер, клеймо, отмечать
Match | соответствие, совпадение,  состязание
malleable | податливый, пластичный
meal | еда
measure | мера, измерять
Merge | объединить, сливаться
moving around files | перемещение файлов
monk | монах
mundane | примитивный, обыденный
miraculously | чудесным образом
mindfulness | осведомленность
**N**|
notice | уведомление, извещение
Noun | существительное
negligence | небрежность, халатность
Necessary | необходимо
**O**|
obvious | очевидный
occasion | повод, событие
over a large| над большим
overwhelm | подавить, сокрушить, ошеломить
Ovarian |  яичник (женское.органы)
Once | однажны
Onto | на, в (поверхность)
ordered (by) | (по) приказу, заказу
**P**|
Past |  Прошлое, мимо
Particular | особый, конкретный, специфический
pattern | шаблон, структура, модель
patience | терпение
perform| выполнять, исполнять  
Perhaps| возможно
Pleased | довольный
Punishment | наказание 
pout | дуться, гримаса
Ponies | пони
pretty pissed | довольно разозлилась
pressure | давление
provide | обеспечивать, представлять
probably | вероятно, наверное
Pristine | нетронутый, чистый 
put | класть, ставить
**Q**|
quiet | тихий, спокойный
Quite | довольно, весьма
**R**|
races | скачки, бег
rate | показатель, темп, ставка
raise | поднимать (object)
rearrange | переставить, изменить
recognize |  узнать, признать
relatively | относительно
restlessness | беспокойство (res(t)ləsnəs)
require | требовать
Round | раунд, цикл, вокруг
rather | довольно, скорее
Recently | недавно, в последнее время
rest | отдых, остаток
Reached | достигать
Reduce | сократить, уменьшать
Refuse | отказать, отказаться
reply | отвечать, ответ
reign | царствовать
rely | полагаться, надеяться
repossess | изымать за долги, возвращать во владение
responsibility | ответственность, обязанность.
retain | удерживать, сохранять
remove | удалить, удалять
regret | сожалеть, раскаиваться
Regex | регулярные выражения
Rise | подниматься, расти (subject)
rigorous | тщательный, строгий
rumors | слухи
**S**|
sagging | провисание, прогиб
same | , одинаковый, тот же
software | программное обеспечение
several | несколько
Sentence |  приговорить, предложение
Seem | казаться, выглядеть
side | сторона, бок
slipping |  скользит
Sledge | Сани, нарты
skirt | юбка, подол, край
Skier | лыжник 
sense | смысл, чувство, ощущение
suite | набор, комплект
Suggest | предложить
Surface | поверхность, поверхностный
sustenance | средства к существованию, поддержка
suffocate | душить, задыхаться 
supposed | предлагаемый
sudden | внезапный, неожиданный
shell | оболочки
shape | форма
shut | закрыть, закрытый
shine | сиять, блеск
silly | глупо, простой, слабоумный
script | сценарий
spending | расходы, траты
stem | основа, стрежень, род
stake | ставка, доля, кол
steam | пар, париться
straight | прямо, сразу, немедленно
stag party | холостяцкая вечеринка 
still |по-прежнему, всё  ещё
still | в состоянии, способный
stick | палка, придерживаться
stitch | шить, торчок 
statement | заявление, утверждение , инструкция
soon | скоро, рано
**T**|
tall | высокий, высотой
task| Задача
template | шаблон, образец
tatling | болтовня
than | чем
there | там, здесь
thirsty | жажда, хочу пить
Thing | вещь, предмет
Think | думать, мысль
Themselves | сами, самостоятельно
tightly | плотно, тесно, крепко
tedious | утомительным, нудным
temporary | временный
Trent | палатка, навес
those | тех, этих
thick | густой, толстый
toes | пальцы ног
trousers | брюки 
throw-away programs | одноразовые программы
to express | выражать
trying out | испытания
trick | трюк
tucked | укладывать, укутывать
tuple | кортеж
Twice | дважды, вдвое
**U**|
upon | на, по, в, за (предлог хамелеон)
up-to-date | своевременно
unemployment | безработица
unsettle | расстраивать, выбивать из колеи
unsettled | нерешенный, неурегулированный
unacceptable | неприемлемо(ый)
**V**|
Violence | насилие, жесткость, сила
vile | подлый, мерзкий, отвратительный
variable | переменных
vendor | поставщик
Vowel | гласный звук
**W**|
Warm | Теплый, нагреваться
watcher | наблюдатель
wander | бродить, блуждать
Wretched | Ужасный, жалкий, несчастной
wisdom | мудрость
withdraw | изымать, отзывать
witness | свидетель, свидетельствовать
widths | ширина
Womb| матка, чрево, лоно
wearing | носить, утомительный
well-being |  благополучие, благостостояние
wealth | Богатство
whole | весь, целый
well-suited | хорошо подходит
welfare | благосостояние
which | который
**X**|
X-rays | рентгеновские лучи
**Y**|
Yard | двор, сад, площадка
Yet | еще, тем не менее, кроме того
**Z**|
Zeal | рвение
'''

for huge_letter in string.ascii_uppercase:
    dictionary = re.sub(r'\n' + huge_letter, r'\n' + huge_letter.lower(), dictionary)

print(dictionary)