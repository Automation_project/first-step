#  Вывод вложенных списков в виде таблицы.

picnicItems = [['apples', 'oranges', 'cherries'],
               ['essential', 'negligence', 'impede'],
               ['excited', 'retain', 'implement']]
k = 0  # ключ, то есть номер вложенного списка
v = 0  # элементы во вложенном списке
z = 0

# Определяем максимальную длинну элемента в списке.
for each in picnicItems:  # each - список(n) в списке
    for x in each:  # x - элемент в списке(n)
        if z < len(x):
            z = len(x)

while True:
    if k <= 1:  # Цикл первый, печатаем нулевые элементы в 0 и 1 вложенных списках
        print(picnicItems[k][v].rjust(z), end=' ')
        k += 1
    elif k == 2:  # Выводим 0 элемент из 2 вложенного списке
        print(picnicItems[k][v].rjust(z))
        if v != 2:
            v += 1  # Изменяем переменную отвечающую за элемент во вложенном списке.
            k = 0  # Изменяем переменную, что бы начать цикл с самого начала.
        else:
            break



