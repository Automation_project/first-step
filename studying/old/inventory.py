first_inventory = {'rope': 1, 'torch': 6, 'gold coin': 42, 'dagger': 1, 'arrow': 12, 'probably': 2, 'supposed': 10}

loot = ['retain', 'probably', 'repossess', 'supposed']

for each in loot:
    if first_inventory.get(each, 0) == 0:
        first_inventory.setdefault(each, 1)
    else:
        v = first_inventory.get(each, 0) + 1
        first_inventory[each] = v


def display_inventory(dictionary):
    print('Inventory:')
    total = 0
    for k, v in dictionary.items():
        print(v, k)
        total += v
    print('Total number of items: ', total)


display_inventory(first_inventory)
