#  Принимает перечень англ. слов с переводом из буфера обмена,
#  сохраняет их в таблицу адаптированную под markdown, и в буфер обмена.
#  Создает два текст. файла  "значение-перевод" и "перевод-значение" адаптированных для импорта в Anki.

import pyperclip
import re
import string
technical_dictionary = open('C:\\Users\\Work\\Documents/technical dictionary.txt', 'r+')
dictionary = technical_dictionary.read()
new_words = re.sub(r'  |\. ', ' | ', pyperclip.paste())
new_words = '\n' + new_words
words_in_new_words = re.findall(r'\n.*', new_words)

for each_words in words_in_new_words:
    only_en_w = re.search(r'\n' + r'.*' + r'[|]', each_words)
    have_match = re.search(only_en_w.group()[:-1], dictionary)
    print(have_match)  # None если нет совпадения.
    if not have_match:
        dictionary += each_words
        print(each_words)

for first_huge_letter_in_en_w in string.ascii_uppercase:
    dictionary = re.sub(r'\n' + first_huge_letter_in_en_w, r'\n' + first_huge_letter_in_en_w.lower(), dictionary)

list_of_translated_words = ['words | translate', '\n--|--|']

for thirst_letter in string.ascii_lowercase:
    huge_title_letters = ('\n''**' + thirst_letter.upper() + '**' + '|')
    list_of_translated_words.append(huge_title_letters)

    empty_second_letter = re.findall((r'\n' + thirst_letter + r'\W') + r'.*', dictionary)
    if empty_second_letter:
        for match_e_s_l in empty_second_letter:
            list_of_translated_words.append(match_e_s_l)

    for letter in string.ascii_lowercase:  # sec. letter iteration
        second_letter = re.findall((r'\n' + thirst_letter + letter) + r'.*', dictionary)
        if second_letter:
            for match_s_l in second_letter:
                list_of_translated_words.append(match_s_l)


final_tech_dict = ''
for each_tr_w in list_of_translated_words:
    final_tech_dict = final_tech_dict + each_tr_w

technical_dictionary.seek(0)
technical_dictionary.write(final_tech_dict)
pyperclip.copy(final_tech_dict)
adapt_for_Anki = re.sub(r"(words | translate)", '', final_tech_dict)
adapt_for_Anki = re.sub(r"(\W\W\D\W\W\W)", '', adapt_for_Anki)
dictionary_for_Anki = re.sub(r"-\|", '', adapt_for_Anki)
my_file = open('C:\\Users\\Work\\Documents/dictionary for Anki.txt', 'w+', encoding='utf-8')
my_file.write(dictionary_for_Anki)

russian_words = re.findall(r'[|]' + r'.*', dictionary_for_Anki)
english_words = re.findall(r'\n' + r'.*' + r'[|]', dictionary_for_Anki)

inverse_dictionary = ''
for ru_w, en_w in zip(russian_words, english_words):
    new_line = '\n{} | {}'.format(ru_w[2:].lower(), en_w[1:-2])
    inverse_dictionary += new_line

change_inverse_dictionary = open('C:\\Users\\Work\\Documents/inverse dictionary.txt', 'w+', encoding='utf-8')
change_inverse_dictionary.write(inverse_dictionary)