import pprint

everyday_products = ({
    'tea': ['green', 'black', 'yellow'],
    'chocolate': ['white', 'black', 'brown'],
    'tesa': ['green', 'black', 'yellow'],
    'tesda': ['green', 'black', 'yellow'],
    'teaa': ['green', 'black', 'yellow'],
    'tewa': ['green', 'black', 'yellow']

})
for i in everyday_products.items():
    pprint.pprint(i)
# print(*list(everyday_products.items()), sep='\n')  # Вывод списка с каждым элементом в новой строке.

# message = 'It was a bright cold day in April, and the clocks were striking thirteen'
# count = {}
# for character in message:
#     count.setdefault(character, 0)  # Ключу присваевается значение 0, если ранее его значение не встречалось.
#     # В ином случае присвоение значения игнорируется самими методом (.setdefault).
#     count[character] = count[character]+1  # выводится значение по ключу из словара count, и прибавляется к нему 1.
# print(count)
