import pprint
import pyperclip
import re
import string


dictionary = '''words | translate
--|--|
**A**|
a lot of | много
a long on | долгий, длинный
abilities | способности
able | способный, в состоянии
across | через, сквозь
acceptance | принятие
advanced | передовой, продвинутый
advice | совет
advertisement | рекламное объявление
afford | доступный, предоставить
against | против, от
agree | согласиться
alert | тревога
alerted | предупредил
allows | позволяет, разрешено
allowed |разрешено
almost | почти
amiss | неправильно, неладно, дурно
anymore | больше (не), уже (не)
anchor | якорь
applicable | применимы(й)
appalled | потрясен
append | добавлять, присоединять
arrays | массивы
arise | возникать, появляться
as | так же, так как, как 
assume | предполагать (под свою ответственность)
assignment | назначение, присвоение
at least | по меньшей (крайней) мере 
attribute | признак, свойство
available | имеется в наличии, доступен
away | прочь
aware | осведомленный
awful | ужасный
**B**|
bar | стойка, планка
barely | едва
batch | партия, группа, пакет
betrayed | предательство
below | ниже
besides | помимо, кроме того.
being | v3, быть, являться в данный момент
belong | принадлежать
binary form | в двоичной форме
blame | вина, ответственность, порицание
blast | взрыв, звук
bottom-up | восходящая, вверх дном, беспорядочная
boundary | граница (areas)
border | граница (country)
boil | нарыв, кипятить, варить
brackets | скобки
breath | дыхание, вдох
broad | широкий, масштабный
bunch | связка, группа
built in | встроенных
**C**|
cab | такси
cape | мыс, плащ, накидка
carry | нести, переносить 
callable | вызываемый
calm | спокойный
cause | причина, дело, вызвать
certain | определенный,  уверенный
cheat | обман, мошенничать
chapter | глава
chase | погоня, преследовать
clause | оговорка, предложение, пункт
complicated | сложный, запутанный
column | столбец, колонка
compile | собирать, составлять
completely | полностью, совершенно
complain | жаловаться
compassion | сострадание 
comma | запятая
common | общий
concern | беспокойство, концерн.
contemptible | презренный
comeuppance | возмездие
considerable | значительный, существенный
conveyance | перевозка
count | считать, подсчёт
coincidence | совпадение
comprehension | понимание
cross-legged | сидящий, скрестив ноги
custom | настраеваемые
cursed | проклятый, отвратительный
cure | лекарство, лечить
**D**|
data types | типы данных
developer | разработчик, разработка
declarations | декларация, объявление
describe | описывать
degree | степень (диплом)
degrees | градусы
depot | склад, хранилище, станция
develop | разработать, развивать
deserve | заслуживать
decipher | расшифровать, разбирать
decide | решить, принимать решение
define | определять
difficult | трудные, затруднительные
dignity | достоинство, титул, звание
distract | отвлечь, отвлекать
district | район
domain | область, сфера
doubt | сомневаться 
drop | уронить, падать,  капля
during | в течении, во время
**E**|
earn | зарабатывать, получать
ease | легкость, простота, удобство
earthquake | землетрясение
edge | край
either | или
elbow | локти, проталкиваться, толкать локтями
enough | достаточно
encourage | поощрять, призывать
enjoyed | наслаждаться
essential | необходимый, важный, основной
estate | Имущество
eventually| в конце концов
even | даже 
events | события
ever wonder | когда нибудь задумывался
extension | расширение, распространение
extensible | расширяемый
external | внешний
explain | объяснять
expect | ожидать, рассчитывать
exceptions | исключения
expressions | выражений
examples | примеры
excited | взолнованы, возбуждающий в восторге
expensive | дорогой, дорогостоящий
**F**|
fares | тарифы
features | особенности, функции
festering | гнойный
few | немного, мало
feet | ноги, стопы, лапы
fired | уволен
fit | подходит, соответствует
flexible | гибкий, свободный 
flow | поток, течь, течение
flush | смываться, прилив, румянец
folks | близкие (друзья, родственники)
fog | туман, мгла
fortunate | удачливый
forward | вперёд, пересылать, нападающий
fraud | мошенничество, обман
fruition | Плоды, осуществление, наслаждение
**G**|
gee | ну и дела
gently | нежно, мягко
glad | радостный, довольный 
guess | предполагал, угадать
guardian | опекун, попечитель
**H**|
hall | зал, коридор, холл
hazardous | опасный, рискованный
handy | удобный
hangover | похмелье
heat | жара, нагревать
heighten | повышать
heavy | тяжелый
high score | рекорд 
hook(ed) | зацепка, крюк (зацепитесь)
**I**|
implement | реализовывать, осуществлять
impede | препятсвовать, затруднять, мешать
improve | улучшать, усовершенствовать
interactively | интерактивно
intolerable | невыносимый, нестерпимый
increase | увеличение (размер)
indefinite | неопределенный
indentation | оступ, углубление
independent | независимый
influence | влияние, воздействовать
injuries | травмы
insert | вставять, внести дополнения
insane | безумный, душевнобольной
instead | вместо
insist | настаивать
insight | интуиция, понимание
instantly | немедленно
inevitably | неизбежно
issues  | вопросы
is done | сделано
item | элемент, пункт
**J**|
jolly | весёлый, добродушный
**K**|
kind | добрый, вид, тип
kind of | вроде как, как будто
**L**|
least | наименее, меньше всего
let | позволять, дать
linking | связывание, соединение
little | мало, немного
list comprehension | списковое включение
loose | свободный, болтающийся, развязывать
look sharp | смотри в оба 
look after | присматривать, приглядывать
**M**|
mark | отметка, маркер, клеймо, отмечать
match | соответствие, совпадение,  состязание
malleable | податливый, пластичный
meal | еда
measure | мера, измерять
merge | объединить, сливаться
miraculously | чудесным образом
mindfulness | осведомленность
moving around files | перемещение файлов
monk | монах
mundane | примитивный, обыденный
multiple | многократный, множественный
**N**|
negligence | небрежность, халатность
necessary | необходимо
notice | уведомление, извещение
noun | существительное
**O**|
obvious | очевидный
occasion | повод, событие
once | однажны
onto | на, в (поверхность)
ordered (by) | (по) приказу, заказу
over a large| над большим
overwhelm | подавить, сокрушить, ошеломить
ovarian | яичник (женское.органы)
**P**|
past | Прошлое, мимо
particular | особый, конкретный, специфический
pattern | шаблон, модель
patience | терпение
perform| выполнять, исполнять  
perhaps| возможно
pleased | довольный
pout | дуться, гримаса
ponies | пони
possible | возможный(ых)
pretty pissed | довольно разозлилась
pressure | давление
provide | обеспечивать, представлять
probably | вероятно, наверное
pristine | нетронутый, чистый 
punishment | наказание 
put | класть, ставить
**Q**|
quiet | тихий, спокойный
quite | довольно, весьма
**R**|
races | скачки, бег
rate | показатель, темп, ставка
raise | поднимать (object)
rather | довольно, скорее
rearrange | переставить, изменить
recognize | узнать, признать
relatively | относительно
restlessness | беспокойство (res(t)l?sn?s)
require | требовать
recently | недавно, в последнее время
rest | отдых, остаток
reached | достигать
reduce | сократить, уменьшать
refuse | отказать, отказаться
reply | отвечать, ответ
reign | царствовать
rely | полагаться, надеяться
repossess | изымать за долги, возвращать во владение
responsibility | ответственность, обязанность.
retain | удерживать, сохранять
remove | удалить, удалять
regret | сожалеть, раскаиваться
regex | регулярные выражения
repeat | повторять, повторение, повторный
rise | подниматься, расти (subject)
rigorous | тщательный, строгий
round | раунд, цикл, вокруг
rumors | слухи
**S**|
sagging | провисание, прогиб
same | одинаковый, тот же
script | сценарий
several | несколько
sentence | приговорить, предложение
seem | казаться, выглядеть
sense | смысл, чувство, ощущение
sequence | последовательность
shell | оболочки
shape | форма
shut | закрыть, закрытый
shine | сиять, блеск
side | сторона, бок
silly | глупо, простой, слабоумный
skirt | юбка, подол, край
skier | лыжник 
slipping | скользит
sledge | Сани, нарты
software | программное обеспечение
soon | скоро, рано
spending | расходы, траты
stem | основа, стрежень, род
stake | ставка, доля, кол
steam | пар, париться
straight | прямо, сразу, немедленно
stag party | холостяцкая вечеринка 
still |по-прежнему, всё  ещё
still | в состоянии, способный
stick | палка, придерживаться
stitch | шить, торчок 
statement | заявление, утверждение , инструкция
suite | набор, комплект
suggest | предложить
surface | поверхность, поверхностный
sustenance | средства к существованию, поддержка
suffocate | душить, задыхаться 
supposed | предлагаемый
sudden | внезапный, неожиданный
such | такой, определенный
**T**|
tall | высокий, высотой
task| Задача
tattling | болтовня
tedious | утомительным, нудным
temporary | временный
than | чем
there | там, здесь
thirsty | жажда, хочу пить
thing | вещь, предмет
think | думать, мысль
themselves | сами, самостоятельно
those | тех, этих
thick | густой, толстый
throw-away programs | одноразовые программы
tightly | плотно, тесно, крепко
to examine | исследовать, рассмотривать
toes | пальцы ног
to express | выражать
trent | палатка, навес
trousers | брюки 
trying out | испытания
trick | трюк
tucked | укладывать, укутывать
tuple | кортеж
twice | дважды, вдвое
**U**|
unemployment | безработица
unsettle | расстраивать, выбивать из колеи
unsettled | нерешенный, неурегулированный
unacceptable | неприемлемо(ый)
upon | на, по, в, за (предлог хамелеон)
up-to-date | своевременно
**V**|
variable | переменных
vendor | поставщик
violence | насилие, жесткость, сила
vile | подлый, мерзкий, отвратительный
vowel | гласный звук
**W**|
warm | Теплый, нагреваться
watcher | наблюдатель
wander | бродить, блуждать
wearing | носить, утомительный
well-being | благополучие, благостостояние
wealth | Богатство
well-suited | хорошо подходит
welfare | благосостояние
whole | весь, целый
which | который
wisdom | мудрость
withdraw | изымать, отзывать
witness | свидетель, свидетельствовать
widths | ширина
womb| матка, чрево, лоно
wretched | Ужасный, жалкий, несчастной
**X**|
x-rays | рентгеновские лучи
**Y**|
yard | двор, сад, площадка
yet | еще, тем не менее, кроме того
**Z**|
zeal | рвение'''


for thirst_letter in string.ascii_lowercase:
    empty_second_letter = re.search(r'^' + thirst_letter + r'.*', dictionary, re.MULTILINE)

    print(empty_second_letter.group())