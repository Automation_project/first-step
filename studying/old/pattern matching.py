import random

all_dishes = []

while True:
    dishes_name = input()
    match dishes_name:
        case '':
            print('Input any dish you want.')
        case 'total order':
            print(all_dishes)
        case 'end':
            break
        case _:
            all_dishes.append(dishes_name)
            print('-----')

total_cost = sum(len(each_dishes) * random.randint(10, 50) for each_dishes in all_dishes)
print(total_cost)


# Новый способ сортировки с 3.10. Разобраться как работает.
#
# run_action = 'alpha 500'.split()
#
# match run_action:
#     case 'alpha' | 'beta' | 'gamma' as action, value:
#         print(f'Go to {action} to {value}')
