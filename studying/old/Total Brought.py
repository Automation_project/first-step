allGuests = {'Alice': {'apples': 5, 'potato': 12},
             'Bob': {'carrot': 3, 'apples': 2},
             }


def total_brought(guests, item):  # функция Всего_принесли с параметрами Гости, предметы.
    number_of_brought = 0  # Количество принесенно
    for k, V in guests.items():  # Итерация по переменным K V в элементах словаря AllGuests.
        # Где k - это имя, а V - словарь привязанный к имени.
        number_of_brought = number_of_brought + V.get(item, 0)
        # Для словаря - V, привязанного к имени - ключу (k) Через метод get,
        # проверяется наличие ключа передаваемого пользователем при вызове функции
        # если ДА, его значение извлекается, и будучи целым числом прибавляется к переменной number_of_brought
        # и сохраняется в ней. При следующем цикле итерации по СЛЕДУЮЩЕМУ ключу, переменная number_of_brought
        # уже содержит ранее сохраненное значение.
        # Тем самым, новое значение извлеченное методом get будет добавлено
        # к ранее сохраненному значению в number_of_brought.
    return number_of_brought  # Возвращает итоговое значение переменной number_of_brought
    # полученной в итоге итерации.


print('Numbers of things being brought: ')

print(' - Apples ' + str(total_brought(allGuests, 'apples')))

print(' - Carrot ' + str(total_brought(allGuests, 'carrot')))

print(' - Cakes ' + str(total_brought(allGuests, ' cakes')))

