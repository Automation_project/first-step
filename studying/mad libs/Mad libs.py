import re

#  2.1 Цель предоставить возможность замены с пропуском предыдущего
#  схожего соотвествия не получилась. re.sub затирает первый попавшийся match. -
#  -  Частично закрыл через размер объектов.
#
#  Файл открывать на чтение.
#  Скопировать данные в переменную.
#  Поиск на соответствие одного из 4 значений.
#  Редактировать текст, пронумерировав каждое соответствие.
#  Вывести преобразованную переменную на экран.
#  Блок 2
#  Предложить заменить, предоставить возможность пропуска замены.
#  Если пропуск, перейти к следующему соответсвию.
#  Если замена, переписать переменную и вывести результат на экран.
#  И перейти к следующему.

source = open('examples.txt')
rearrange_source = source.read()

changing_words = ['ADJECTIVE', 'NOUN', 'VERB']
print(rearrange_source)

for match in re.finditer(r'ADJECTIVE | NOUN | VERB', rearrange_source):
    print('Please input ' + match.group().lower())
    change_each = ' ' + input() + ' '
    if change_each == '  ':
        rearrange_source = re.sub(match.group(), match.group().lower(),
                                  rearrange_source, count=1)
        continue
    else:
        rearrange_source = re.sub(match.group(), change_each,
                                  rearrange_source, count=1)
        print(rearrange_source)


