#  Функция возвращает список, где каждый элемент имеет тип введенных данных(str, int, float).


eggs = []


def change_list(your_list):
    while True:
        print('Input items')
        x = input()
        if x != '':
            try:  # Проверка является ли ли элемент целым числом
                int(x)
            except ValueError:  # Если нет, проверяется как вещественное
                try:
                    float(x)
                except:  # Если нет, добавляется как строка
                    your_list.append(x)
                else:  # Если да, добавляется как вещественное
                    your_list.append(float(x))
            else:  # Если да, добавляется как целое
                your_list.append(int(x))
        else:
            break
        print(your_list)
    print('Your list now contains these elements: :', your_list)


change_list(eggs)

print([type(x) for x in eggs])
