Модуль re
Импортируемый модуль позволяющий находить, редактировать содержимое строк.

Между регулярным выражением и текстов учитывается, в каком регистре записаны буквы.

Содержит 6 наиболее часто используемых методов.

- `re.match(pattern, string)` Ищет по заданному шаблону в ***начале*** строки.

- `re.search(pattern, string)` По всей строке, но возвращает только **первое** найденное совпадение.

- `re.findall(pattern, string)` Возвращает **список** если в условии нет групп, и **кортеж** если есть. 

- `re.split(pattern, string, maxsplit=0)` Разделяет строку по заданному шаблону.

  **maxsplit=0** : Максимальное количество разбиений (остаток исходной строки станет последним элементом результирующего списка).

- `re.sub(pattern, repl, string, count=0, flags = 0)` Ищет шаблон в строке, и заменяет его на указанную подстроку. 

  **count=0** Максимальное количество замен, которое дозволено произвести. Если не указано (0), то будут произведены все возможные замены.

- `re.compile(pattern, string)` Создаёт отдельный объект, который используется для поиска. Избавляет от необходимости переписывать одни и те же выражения.

- `re.finditer(pattern, string)` Итератор всем непересекающимся шаблонам `pattern` в строке `string` (выдаются `match`-объекты);

- ``re.fullmatch(pattern, string, flags=0)`` Вернет объект сопоставления, если вся строка `string` соответствует шаблону регулярного выражения `pattern`

### Примеры

| `.`                  | один любой символ , кроме новой строки (\n)                  | м.л.ко                        | *молоко*, *малако*, И*м0л0ко*Ихлеб                           |
| -------------------- | ------------------------------------------------------------ | ----------------------------- | ------------------------------------------------------------ |
| obj1 **\|** obj2     | и                                                            | `.compile(r"(bat(man|boy))")` | **bat*man***, batvan,batson,**bat*boy***                     |
| `[^]`                | не                                                           |                               |                                                              |
| ^                    | начало строки                                                |                               |                                                              |
| $                    | конец строки                                                 |                               |                                                              |
| *                    | 0+                                                           |                               |                                                              |
| +                    | 1+                                                           |                               |                                                              |
| ?                    | 0 или 1                                                      |                               |                                                              |
| (bat(wo)?man)        | ? указывает на то, что предшествующая ему группа является необязательной частью поиска. |                               |                                                              |
| `{,m}{n,m}`          | От 0 или n до  m совпадений.                                 |                               |                                                              |
| `{n,m}? or *? or +?` | нежадный поиск с вышеперечисленными условиями                |                               |                                                              |
| `^spam`              | Строка должна начинаться символами spam                      |                               |                                                              |
| spam$                | Заканчиваться ими                                            |                               |                                                              |
| `[abc] [^abc]`       | совпадает или не совпадает с любым одиночным символом в скобках |                               |                                                              |
| \d                   | Любая цифра                                                  | СУ\d\d                        | ***СУ35***, ***СУ11***1, АЛ***СУ14***                        |
| \D                   | Любой символ, кроме цифры                                    | 926\D123                      | ***926)123***,  1***926-123***4                              |
| \s                   | Любой пробельный символ (пробел, табуляция, конец строки и т. п.) | бор\sода                      | ***бор ода***, ***бор\nода***, борода                        |
| \S                   | Любой не пробельный символ                                   | \S123                         | ***X123***, ***я123***, ***!123***456, 1 + 123456            |
| \w                   | Любая буква (то, что может быть частью слова), а также цифры и `_` | \w\w\w                        | ***Год***, ***f_3***, ***qwe***rt                            |
| \W                   | Любая не-буква, не-цифра и не подчёркивание                  | com\W                         | ***сом!***, ***сом?***                                       |
| \t                   | Табуляция                                                    |                               |                                                              |
| `[..]`               | Один из символов в скобках, а также любой символ из диапазона `a-b` | [0-9] [0-9A-Fa-f]             | ***12***, ***1F***, ***4B***                                 |
| `[ ]`                | диапазон                                                     |                               |                                                              |
| `[abc-], [-1]`       | Если нужен минус, его нужно указать последним или первым     |                               |                                                              |
| `[*[(+\\\]\t]`       | внутри скобок нужно экранировать только `]` и `\`            |                               |                                                              |
| \b                   | Начало или конец слова (слева пусто или не-буква, справа буква и наоборот).<br/>В отличие от предыдущих соответствует позиции, а не символу | \bвал                         | ***вал***, перевал, Перевалка                                |
| \B                   | Не граница слова: либо и слева, и справа буквы, либо и слева, и справа НЕ буквы | \Bвал \Bвал\B                 | пере***вал***, вал, Пере***вал***ка       перевал, вал, Пере***вал***ка |



#### КОНСТАНТЫ (flags)

- `re.IGNORECASE` или `re.I` в. ф.  `(?i)`

  Игнорирование регистра букв. 

  ```python
   robo = re.compile(r'abc', re.I)
  ```

- `re.VERBOSE` или `re.X` в. ф. `(?x)`

  Игнорирование пробелов и комментариев в строке регулярного выражения.

  

  ```python
  phone = re.compile(r''' (
  (\d{3} | \ (\d{3}\))?`
  (\s|-|\.?))''', re.VERBOSE)
  ```

- `re.DOTALL` или `re.S` в. ф. `(?s)`

  Теперь точка также соответствует любому символу ВКЛЮЧАЯ новую строку.

- `re.MULTILINE` или `re.M` в. ф. `(?m)`

  1) символ шаблоне `'^'` в шаблоне совпадает с позицией начала строки и с позицией начала каждой строки, сразу после каждой новой строки.
  2) символ шаблона `'$'` совпадает с позицией конца строки и с позицией конца каждой строки, непосредственно перед каждой новой строкой.

- `re.DEBUG`

  Показывает отладочную информацию о скомпилированном выражении

- `re.ASII` или `re.A` в. ф. `(?a)`

  Заставляет `\w`, `\W`, `\b`, `\B`, `\d`, `\D`, `\s` и `\S` выполнить только ASCII соответствие вместо полного соответствия
  
  **GROUPS**
  
  Что бы получить группу, достаточно взять в скобки внутри "строки"
  
  ```python
  english_words = re.findall(r'(.*)' + r'[|]', dictionary_for_Anki, re.M)
  print(english_words)
  ```
  
  
  
  ***Прочее***
  
  * Пример: по паттерну условно подходят пары слов со словами под номерами 5и6 и 6и7, метод возвращает только пару слов 5и6, игнорируя пару 6и7, после чего дальше идёт по строке. Есть ли какая возможность методом re обойти данный момент? 
  
    overlapping=True.
  
  
  
  * Также есть методы `start()` и `end()` для того, чтобы узнать начальную и конечную позицию найденной строки.
  
  ```python
  result = re.match(r'AV', 'AV Analytics Vidhya AV')
  print result.start()
  print result.end()
  
  Результат:
  0
  2
  ```
