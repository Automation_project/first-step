**For**



Итерация по двум объектам одновременно используя Функцию  **zip** 

```python
x = ''
for a, b in zip(russian, english):
    new_line = '\n{} | {}'.format(a[2:], b[1:-2])
    x += new_line
```



```python
(x for x in s)
print(*k)
---------------
(x for x in s)
```

```python
[x for x in s]
print(k)
-------------
['h', 'e', 'l', 'l', 'o']
```









 
