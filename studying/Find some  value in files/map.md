map

```python
map(function, iterable, iterable 2, iterable 3, ...)
```

Встроенная в Python функция `map()` используется для применения функции к каждому элементу итерируемого объекта (например, [списка](https://www.digitalocean.com/community/tutorials/understanding-lists-in-python-3) или [словаря](https://www.digitalocean.com/community/tutorials/understanding-dictionaries-in-python-3)) и возврата нового итератора для получения результатов. Функция `map()` возвращает объект map (итератор), который мы можем использовать в других частях нашей программы. Также мы можем передать объект map в функцию `list()` или другой тип последовательности для создания итерируемого объекта.

Можно добавлять несколько итерируемых объектов.

