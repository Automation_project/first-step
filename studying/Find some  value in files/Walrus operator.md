### *Моржовый оператор*

Оператор, который позволяет присвоить значение переменной и вернуть это значение.

`name:= expression`

Примеры:

```python
print(num := 15)
-----------
15
```

```python
while (value := input('Please enter something: ') !=' '):
    print('Nice')
------------
Please enter something: Kek
Nice
Please enter something: 12314
Nice
```

