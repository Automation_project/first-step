#### STAR Python(*)
`*` и `**`  являются операторами. Существует несколько способов использования.

- Для распаковки итерируемого объекта в аргументы функции.

```python
something_about_music = ['concert', 'guitar', 'lyrics', 'band']
print (*something_about_music)
-- -- -- --
concert guitar lyrics band
```

- Оператор `**` исполняет схожую функцию, только с именованными аргументами (СЛОВАРЯМИ). Рассмотрим на примере словаря.

```python
song = {'guitar': "2", 'band': "3", 'lyrics':  "10"}
concert = "We need {guitar} guitar, {band} band, and {lyrics} lyrics for them.".format(**song)
print (concert)
-- -- -- --
We need 2 guitar, 3 band, and 10 lyrics for them.
```

- Начиная с Python 3.5 `*` и `**` можно использовать несколько раз при вызове функции. 
>Однако необходимо учитывать при использовании оператора `**`, что функции не могут иметь несколько одинаковых именованных аргументов, поэтому ключи в словарях не должны пересекаться, иначе будет выброшено исключение.

```python
song = ['guitar', 'band', 'lyrics']
numbers = [2,3,10]
print (*song, *numbers)
-- -- -- -- -- -- --
guitar band lyrics 2 3 10
```

```python
band = {'famous': ["Kevin", "Justin"], 'album': "Number One", 'composer': "Bethoven" }
thing = {'place': "Las Vegas", 'Date': "20 october 2019", 'time': "evening" }
program = "Today {Date} in {place} we happy meet our stars {famous} and {composer}" \
" with records {album}. Come to {time}.".format(**band,**thing)
print (program)
-- -- -- -- --
Today 20 october 2019 in Las Vegas we happy meet our stars ['Kevin', 'Justin'] and Bethoven with records Number One. Come to evening.
```

- Для упаковки аргументов, переданных в функцию.
>Вообще не понятно что это и зачем.

- Позиционные аргументы с только именованными аргументами
>Вообще не понятно что это и зачем.

- Только именованные аргументы без позиционных
>Вообще не понятно что это и зачем.

- Звёздочки для распаковки
>Насколько я понимаю таким образом из переменной можно выводить необходимые элементы.

```python
fruits = ['lemon', 'pear', 'watermelon', 'tomato']
first, second, *remaining = fruits
remaining
['watermelon', 'tomato']
first, *remaining = fruits
remaining
['pear', 'watermelon', 'tomato']
first, *middle, last = fruits
middle
['pear', 'watermelon']
```

>Вложенная распаковка.

```python
fruits = ['lemon', 'pear', 'watermelon', 'tomato']
((first_letter, *remaining), *other_fruits) = fruits
remaining
['e', 'm', 'o', 'n']
other_fruits
['pear', 'watermelon', 'tomato']
```
```python
fruits = ['lemon', 'pear', 'watermelon', 'tomato']
[[*remaining], *last_fruits] = fruits
print(remaining)
['l', 'e', 'm', 'o', 'n']
```













