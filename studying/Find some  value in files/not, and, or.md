`not` has highest precedence, so its operation is performed first.

`and` is performed next.

`or` has the lowest precedence, so it is performed last.