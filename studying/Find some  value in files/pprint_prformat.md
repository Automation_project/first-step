
https://docs-python.ru/standart-library/modul-pprint-python/funktsija-pformat-modulja-pprint/

https://pyneng.readthedocs.io/ru/latest/book/12_useful_modules/pprint.html

```python
import pprint
cats = [{'name': 'sophia', 'adress': 'ufa'}, {'name': 'Kate', 'adress': 'Moscou'}]
pprint.pformat(cats)
"[{'name': 'sophia', 'adress': 'ufa'}, {'name': 'Kate', 'adress': 'Moscou'}]"
file_obj = open('myGirls.py', 'w')
file_obj.write('girls = ' + pprint.pformat(cats) + '\n')
83
file_obj.close()
```

Словарь сохраненный в .py файле с помощью модуля pprint.pformat

Страница 234.

```python
import myGirls
myGirls.cats
[{'name': 'sophia', 'adress': 'ufa'}, {'name': 'Kate', 'adress': 'Moscou'}]
myGirls.cats[0]
{'name': 'sophia', 'adress': 'ufa'}
myGirls.cats[0]['name']
'sophia'
```

