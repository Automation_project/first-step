## *Pattern matching*

### Шаблонное сопоставление

```python
match some_expression:
    case pattern_1:
        ...
    case pattern_2:
        ...
```

Сами шаблоны разбиты на несколько групп:

- Literal Patterns
- Capture Patterns
- Wildcard Pattern
- Constant Value Patterns
- Sequence Patterns
- Mapping Patterns
- Class Patterns

**Совпадение значения в виде слова или прочего**.

```python
import random
alphabet = chr(random.randint(97, 99))
a = 1
b = 'string'
c = 5, 6, 8

match alphabet:
    case 'a':
        print(a)
    case 'b':
        print(b)
    case _:
        print(c)
```

**Проверка совпадения нескольких значений в списке по первому агрументу** 

```python
run_action = 'alpha 500'.split()
match run_action:
    case 'alpha' | 'beta' | 'gamma' as action, value:
        print(f'Go to {action} to {value}')
--------------
Go to alpha to 500
```



### Literal Patterns

Паттерн Literal, как намекает нам название, предполагает сопоставление между собой ряда значений, а именно строк, чисел, булевых значений и None.

Это выглядит как `string == 'string'`, используется метод `__eq__`.

```python
match number:
    case 42:
        print('answer')
    case 43:
        print('not answer')
```

### Capture Patterns

Шаблон захвата позволяет связать переменную с заданным в шаблоне именем и использовать это имя внутри локальной области видимости.

```python
match greeting:
    case "":
        print('Hello my friend')
    case name:
        print(f'Hello  {name}')
```

### Wildcard Pattern

Если вариантов сопоставления слишком много, то можно использовать` _ `, что является неким значением по умолчанию и будет совпадать со всеми элементами в конструкции `match`

```python
match number:
    case 42:
        print("Its’s forty two")
    case _:
        print("I don’t know, what it is")
```

### Constant Value Patterns

При использовании констант нужно использовать dotted names, к примеру перечисления, иначе сработает паттерн захвата.

```python
OK = 200
CONFLICT = 409

response = {'status': 409, 'msg': 'database error'}
match response['status'], response['msg']:
    case OK, ok_msg:
        print('handler 200')
    case CONFLICT, err_msg:
        print('handler 409')
    case _:
        print('idk this status')
```


И ожидаемый результат будет не самым очевидным.

### Sequence Patterns

Позволяет сопоставлять списки, кортежи и любые другие объекты от `collections.abc.Sequence`, кроме `str`, `bytes`, `bytearray`.

```python
answer = [42]
match answer:
    case []:   
        print('i do not find answer')
    case [x]:
        print('asnwer is 42')
    case [x, *_]:
        print('i find more than one answers')
```

Теперь нет необходимости каждый раз вызывать `len()` для проверки количества элементов в списке, так как будет вызван метод `__len__`.

### Mapping Patterns

Эта группа немного похожа на предыдущую, только здесь мы сопоставляем словари, или, если быть точным, объекты типа `collections.abc.Mapping`. Их можно достаточно неплохо сочетать друг с другом.

```python
args = (1, 2)
kwargs = {'kwarg': 'kwarg', 'one_more_kwarg': 'one_more_kwarg'}

def match_something(*args, **kwargs):
    match (args, kwargs):
        case (arg1, arg2), {'kwarg': kwarg}:
            print('i find positional args and one keyword args')
        case (arg1, arg2), {'kwarg': kwarg, 'one_more_kwarg': one_more_kwarg}:
            print('i find a few keyword args')
        case _:
            print('i cannot match anything')

match_something(*args, **kwargs)
```

И всё бы ничего, но есть особенность. Этот паттерн гарантирует вхождение этого ключа (ключей) в словарь, но длина словаря не имеет значения. Поэтому на экране появится i *find positional args and one keyword args.*

### Class patterns

Что касается пользовательских типов данных, то здесь используется синтаксис, схожий с инициализацией объекта.

Вот как это будет выглядеть на примере дата-классов:

```python
from dataclasses import dataclass

@dataclass
class Coordinate:
    x: int
    y: int
    z: int

coordinate = Coordinate(1, 2, 3)
match coordinate:
    case Coordinate(0, 0, 0):
        print('Zero point')
    case _:
        print('Another point')
```

Также можно использовать `if`, или так называемый `guard`. Если условие ложно, то сопоставление с шаблоном продолжится. Стоит отметить, что сначала происходит сопоставление с шаблоном, и только после этого проверяется условие:

```python
case Coordinate(x, y, z) if z == 0:
    print('Point in the plane XY')
```