Строковые методы **rjust**, **ljust**, **center**.

Возвращают версию строки, измененную за счет вставки пробелов или других знаков. Аргументом является полная длинна "новой" строки.



**rjust( )**

```python
print('hello'.rjust(10))
------
     Hello
```

**ljust( )**

```python
print('Hello'.ljust(10, '*'))
------
Hello*****
```

**center**

```python
print('Hello'.center(11, '^'))
------
^^^Hello^^^
```

