lambda

Анонимные функции, не описанные в пространстве имён. (По сути одноразовые)

lambda <аргументы> : <возвращаемое выражение>

`print((lambda x: x +3)(3))` => 6

lambda аргументы : возвращаемое выражение

lambda x, y: x + y

```python
print((lambda x: x + 1)(2))

>>> f = lambda x: x * x
>>> f(5)
25
```



```python
users = [ 'Jon Dow 23', 'Peeter North 43', 'Greg Olsen 35']
users.sort(key = lambda x: x.split(' ')[-1])
print(users)
```

