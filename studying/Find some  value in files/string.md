#### Строки

Методы.

- [Метод str.capitalize(), первая буква в строке заглавная.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-capitalize/)

- [Метод str.casefold(), сворачивает регистр строки.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-casefold/)

- [Метод str.center(), выравнивает строку по центру.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-center/)

- [Метод str.count(), считает совпадения в строке.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-count/)

- [Метод str.encode(), переводит строку в байты.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-encode/)

- [Метод str.endswith(), совпадение с концом строки.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-endswith/)

- [Метод str.expandtabs(), меняет табуляцию на пробел.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-expandtabs/)

- [Метод str.find(sub,start,end), индекс первого совпадения в строке.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-find/)

- [Метод str.format(), форматирует строку.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-format/)

- [Метод str.format_map()](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-format-map/)

- [Метод str.index(), индекс первого совпадения подстроки.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-index/)

- [Метод str.isalnum(), строка состоит из цифр и букв.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-isalnum/)

- [Метод str.isalpha(), строка состоит только из букв.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-isalpha/)

- [Метод str.isascii(), все символы в строке являются ASCII.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-isascii/)

- [Метод str.isdecimal(), проверяет строку на десятичное число.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-isdecimal/)

- [Метод str.isdigit(), строка состоит только из цифр.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-isdigit/)

- [Метод str.isidentifier() проверяет строку на идентификатор Python.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-isidentifier/)

- [Метод str.islower( ), проверяет строку на нижний регистр.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-islower/)

- [Метод str.isnumeric(), проверяет строку на числовые символы.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-isnumeric/)

- [Метод str.isprintable(), проверяет на доступность для печати.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-isprintable/)

- [Метод str.isspace(), является ли строка пробелом.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-isspace/)

- [Метод str.istitle(), проверяет наличие заглавных букв в словах.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-istitle/)

- [Метод str.isupper(), проверяет строку на верхний регистр.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-isupper/)

- [Метод str.join(), объединяет список строк.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-join/)  `','.join['F', 'B', 'I']`

- [Метод str.ljust(), ровняет строку по левому краю.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-ljust/)

- [Метод str.lower(), строку в нижний регистр.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-lower/)

- [Метод str.lstrip(chars), обрезает символы в начале строки.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-lstrip/)

  - Если `chars` не задан или `None`, то по умолчанию метод `str.lstrip()` удаляет пробелы **в начале строки**.

- [Метод str.maketrans(), таблица символов для str.translate().](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-maketrans/)

- [Метод str.partition(), делит строку по первому совпадению.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-partition/)

- [Метод str.removeprefix(), удаляет префикс строки.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-removeprefix/)

- [Метод str.removesuffix(), удаляет суффикс строки.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-removesuffix/)

- [Метод str.replace(old, new , count), меняет подстроку/символ в строке.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-replace/)

- [Метод str.rfind(), индекс последнего совпадения подстроки.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-rfind/)

   int(-1), когда подстрока не найдена.

- [Метод str.rindex(), индекс последнего совпадения в строке.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-rindex/)

   Возвращает ValueError, когда подстрока не найдена.

- [Метод str.rjust(), ровняет строку по правому краю.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-rjust/)

- [Метод str.rpartition(), делит строку по последнему совпадению.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-rpartition/)

- [Метод str.rsplit(), делит строку с права.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-rsplit/)

- [Метод str.rstrip(), обрезает символы на конце строки.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-rstrip/)

- [Метод str.split(sep=None, maxsplit=-1), делит строку по подстроке.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-split/) 

  sep - разделитель, maxsplit - сколько раз делить строку, по умолчанию  неограниченно (-1).

- [Метод str.splitlines(keepends), делит текст по символу '\n'.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-splitlines/)

  `keepends` - [`bool`](https://docs-python.ru/tutorial/osnovnye-vstroennye-tipy-python/bool-logicheskij-tip-dannyh/), если `True` - разрывы строк не будут вырезаться.

  В отличие от [`str.split()`](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-split/), когда задается строка-разделитель `sep`, метод `str.splitlines()` возвращает пустой список `[]` для пустой строки, а разрыв строки не приводит к дополнительной строке:

- [Метод str.startswith(prefix, start,end), совпадение с началом строки.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-startswith/) `'smartphone'.startswitch('smart')`

  **str. endswith()**  Совпадение с концом строки. Выдает True. prefix - (кортеж, символ или подстрока).

  При вызове без аргументов бросает исключение `TypeError` (требуется как минимум `1` аргумент, передано `0`).

- [Метод str.strip(), обрежет строку с обоих концов.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-strip/) Например `.strip('.,-*!')`позволяет очистить строку от точек, запятых и прочих знаков.

- [Метод str.swapcase(), сменит регистр символов в строке.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-swapcase/)

- [Метод str.title(), каждое слово с заглавной буквы.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-title/)

- [Метод str.translate(), транслирование строки.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-translate/)

- [Метод str.upper(), переведет строку в верхний регистр.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-upper/)

- [Метод str.zfill(), дополнит строку нулями.](https://docs-python.ru/tutorial/operatsii-tekstovymi-strokami-str-python/metod-str-zfill/)

- ```pyt
  str.punctuation (!"#$%&'()*+, -./:;<=>?@[\]^_`{|}~)
  ```

  Содержит все знаки пунктуации.

- Способ получения только цифр из строки

  ```python
  s = 'abc12321cba'
  print(s.translate({ord(i): None for i in 'abc'}))
  ```

- Модификатор `.3f` выводит результат выражения со значением 3 цифр после запятой.

- Параметр `end` позволяет изменить действие программы, после вывода строки. По умолчанию создаётся новая строка `\n` . Если изменить на `end ='!'`, то после вывода  строки, следующие данные  будет начинаться с **!**, и появятся  на той же строке.



 ## Срезы строк.

[start:stop:step]

1) Срез от символа с конца строки до конца строки.

```python
string = "write about something"
print(string[-5:])
-- -- -- -- -- --
thing
```

Пробелы между словами воспринимаются как элементы, и при выводе учитываются программой.

​	2.Если Начальная точка среза больше конечной `str[4:2]` Возвращает пустую строку `''`

4) Изменение содержимого сторки, путем копирования части исходной и добавления необходимых данных.

```python
string = "write about something"
string = string[0:5] + ' or say ' + string[6:21]
print(string)
-- -- -- -- -- --
write or say about something
```



## Форматирование строк

***Первый способ***

```python
something = "%s - it is not a problem. (%s)(%d)"
print (something % ("twisted_ankle","Gregory House", 1945)
-- -- -- -- -- -- --
twisted_ankle - it is not a problem. (Gregory House)(1945)
       
func = 'TET'
string = 'blabla(%s)laaa' % func
print(string)
---------------
blabla(TET)laaa
```

`%s` - позволяет подставить в указанное место строку. `%d` - число. `% func` - Функцию равную строке или числу.

**Второй способ**

```python
print (" Today  ({})  twisted my {}.".format(24.03,'wirst'))
-- -- -- -- -- --
Today (24.03)  twisted my wirst.
```

Строка включает неограниченное количество значений  {}, после чего с помощью метода `.format` устанавливаются необходимые значения.

**Третий способ**

```python
print (" How much {insurance} you {cost} can {cost} fo rent.".format(insurance = 22, cost = 'different'))
-- -- -- -- -- -- --
How much  22  you different can  different fo rent.
```

В результате используя внутренние переменные мы можем указывается или изменяя одно значение менять сразу несколько значений в строке (связанных с переменной)

**Четвертый способ** (работает начиная с версии  python 3.6)

```python
 first_item = 'air-condition'
 second_item = 'lights'
 place = 'boot'

 print (f"Execusme, can you poot {first_item, second_item} in that {place}.")
-- -- -- -- -- -- -- -- --
 Execusme, can you poot ('air-condition', 'lights') in that boot.
```

Используя префикс `f` мы можем просто указав её в начале, и в процессе добавляя ранее определенные  имена переменных выводить необходимые данные.

