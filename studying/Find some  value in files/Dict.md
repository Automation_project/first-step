### Словари.
Словари представляют из себя последовательность ключ-значение.
Это означает при необходимости вывести некие значения, достаточно ввести имя словаря плюс ключ. Рассмотрим на примере.

## Методы словарей

**dict.clear**() - очищает словарь.

**dict.copy**() - возвращает копию словаря.

classmethod **dict.fromkeys**(seq[, value]) - создает словарь с ключами из seq и значением value (по умолчанию None).

**dict.get**(key[, default]) - возвращает значение ключа, но если его нет, не бросает исключение, а возвращает default (по умолчанию None).

**dict.items**() - возвращает пары (ключ, значение).

**dict.keys**() - возвращает ключи в словаре.

**dict.pop**(key[, default]) - удаляет ключ и возвращает значение. Если ключа нет, возвращает default (по умолчанию бросает исключение).

**dict.popitem**() - удаляет и возвращает пару (ключ, значение). Если словарь пуст, бросает исключение KeyError. Помните, что словари не упорядочены.

**dict.setdefault**(key[, default]) - возвращает значение ключа, но если его нет, не бросает исключение, а создает ключ со значением default (по умолчанию None).

**dict.update**([other]) - обновляет словарь, добавляя пары (ключ, значение) из other. Существующие ключи перезаписываются. Возвращает None (не новый словарь!).

**dict.values**() - возвращает значения в словаре.

```python

everyday_products = ({
    'tea': ['green', 'black'],
    'chocolate': ['white', 'black']
})
```

Или другой метод.

```python
everyday_products = dict(
    tea=['green', 'black'],
    chocolate=['white', 'black'])
print(everyday_products)
```

**Добавление и удаление значений в словарь.**

- Добавление.

```python
items = ({'backpack': ['tea', 'frog'], 'bag': ['pen', 'clock']})
items['suitcase'] = 'yet', 'still'
pprint.pprint(items)
---------------
{'backpack': ['tea', 'frog'],
 'bag': ['pen', 'clock'],
 'suitcase': ('yet', 'still')}
```

-  **.update** Если необходимо добавить несколько ключей

```python
items = ({'backpack': ['tea', 'frog'], 'bag': ['pen', 'clock']})

items.update({
    'suitcase': ['yet', 'still'],
    'trunk': 'wheel', 'crowbar'
})
--------------
{'backpack': ['tea', 'frog'],
 'bag': ['pen', 'clock'],
 'suitcase': ['yet', 'still'],
 'trunk': ('wheel', 'crowbar')}
```

- **.setdefault** В случае если необходимо сперва проверить словарь на наличие ключа, а затем добавить этот ключ используется метод 

```python
items = ({'backpack': ['tea', 'frog'], 'bag': ['pen', 'clock']})
items.setdefault('case', ['FROG', ' TEA'])
---------------
{'backpack': ['tea', 'frog'], 'bag': ['pen', 'clock'], 'case': ['FROG', ' TEA']}
```
Если ключ присутствует в словаре, выводится значение ключа.

```python
items = ({'backpack': ['tea', 'frog'], 'bag': ['pen', 'clock']})
print(items.setdefault('backpack', ['FROG', ' TEA']))
-- -- -- --
['tea', 'frog']
```

- Замена.

```python
items = ({'backpack': ['tea', 'frog'], 'bag': ['pen', 'clock']})
items['bag'] = 0
-- -- -- -- --
{'backpack': ['tea', 'frog'], 'bag': 0}
```

- **del** Удаление.

```python
items = ({'backpack': ['tea', 'frog'], 'bag': ['pen', 'clock']})
del items['backpack']
--------------
{'bag': ['pen', 'clock']}
```

- **.pop** Что бы удалить ключ и вернуть его значение. Если ключи одинаковые, удаляет и возвращает ПОСЛЕДНЮЮ пару (ключ, значение).

```python
items = ({'backpack': ['tea', 'frog'], 'bag': ['pen', 'clock']})
items.pop('backpack')
print(items)
----------------
['tea', 'frog']
{'bag': ['pen', 'clock']}
```
**Итерация**
**get** - Добавить описание.
Метод get() возвращает значение для указанного ключа, если ключ находится в словаре.

```python
items = ({'backpack': ['tea', 'frog'], 'bag': ['pen', 'clock']})
trunk = items.get('bag')
--------
['pen', 'clock']
```

Итерация предоставляет возможность вывести некие значения присутствующие в словаре.
Возвращаемые этими методами значения не являются истинными списками: их нельзя изменить, и они не имеют метода **append**, однако эти данные можно использовать в цикле `for`.

```python
apartments = dict(
    living_room=['sofa', 'coffee table'],
    kitchen=['stove', 'fridge'])
print(apartments.items())
--------
dict_items([('living_room', ['sofa', 'coffee table']), ('kitchen', ['stove', 'fridge'])])
```

- Если необходима итерация по ключам и значениям, используется метод `.items()`


```python
everyday_products =({'tea':['green', 'black'], 'chocolate' :['white', 'black']})

for key, value in everyday_products.items():
    print ('{} - {}'.format(key, value))
-- -- -- --
tea - ['green', 'black']
chocolate - ['white', 'black']
```

- Если по значениям, метод `.values()`

```python
everyday_products = dict(
tea=['green', 'black'],
chocolate =['white', 'black'])
for value in everyday_products.values():
    print (value)
-- -- -- --
['green', 'black']
['white', 'black']
```

- Вывод определенного элемента из значения в списке.

```python
apartments = ({
    'living_room': ['sofa', 'coffee table'],
    'kitchen': ['stove', 'fridge']})
print(apartments['kitchen'][0])
-- -- -- --
stove
```

- Вывод всех элементов из значения в списке.

```python
apartments = dict(
    living_room=['sofa', 'coffee table'],
    kitchen=['stove', 'fridge'])

first_customer = ['I see you have a nice {}.'.format(x) for x in apartments['kitchen']]

print('\n'.join(first_customer))
-- -- -- -- --
I see you have a nice stove.
I see you have a nice fridge.
```



- fromkeys

  ```python
  >>> d = dict.fromkeys(['a', 'b'])
  >>> d
  {'a': None, 'b': None}
  >>> d = dict.fromkeys(['a', 'b'], 100)
  >>> d
  {'a': 100, 'b': 100}
  ```

- генераторов словарей

  ```python
  >>> d = {a: a ** 2 for a in range(7)}
  >>> d
  {0: 0, 1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36}
  ```

  

- **dict.fromkeys**(seq[, value]) - создает словарь с ключами из seq и значением value (по умолчанию None).

```python
hero = 'harry', enemy = 'Lord V', guide = 'Dumbledore'
characters = {hero, guide, enemy}
```

В таком виде это не словарь, а множество. И вывод элементов через `print` будет хаотичным. 
