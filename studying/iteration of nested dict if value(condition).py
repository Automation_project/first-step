# Компания выводится если во вложенном словаре нет значений меньше 9.

companies = {'CoolCompany': {'Alice': 33, 'Bob': 22, 'Frank': 29},
             'CheapCompany': {'Ann': 14, 'Lee': 9, 'Christi': 7}}

illegal = [x for x in companies if any(y < 9 for y in companies[x].values())]
print(illegal)