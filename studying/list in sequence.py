#  Вывод вложенных списков  в виде последовательности с учётом длинны значений.

picnicItems = [['apples', 'oranges', 'cherries'],
               ['essential', 'negligence', 'impede'],
               ['excited', 'retain', 'implement']]

v = 0

for lists in picnicItems:  # Определяет наидлиннейшее значение среди всех списков.
    for values in lists:
        if v < len(values):
            v = len(values)

Number_of_nested_lists = 0
for nested_list in picnicItems:
    Number_of_nested_lists += 1
    if Number_of_nested_lists >= 2:  # Создаёт разрыва между списками.
        print('', end='\n')
    for worth in nested_list:
        print(worth.rjust(v))  # Выравнивает таблицу по длине.
