import pyperclip

title = pyperclip.paste()

new_string = ''

for each in title.split()[2:4]:
    new_string += each + ' '

pyperclip.copy(new_string)
