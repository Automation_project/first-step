import pprint
import sqlite3 as sq
import re
my_file = open('dictionary for Anki.txt', 'r', encoding='utf-8')
dictionary_for_Anki = my_file.read()
russian_words = re.findall(r'\| ' + r'(.*)', dictionary_for_Anki)
english_words = re.findall(r'(^.*)' + r' \|', dictionary_for_Anki, re.M)


# with sq.connect('technical dictionary.db') as con:
#     cursor = con.cursor()
#     cursor.execute("DROP TABLE IF EXISTS words")
#     cursor.execute("""CREATE TABLE IF NOT EXISTS words (
#     id INTEGER PRIMARY KEY,
#     english TEXT,
#     translate TEXT
#     )""")
#     # cursor.execute("INSERT INTO words (english,translate) VALUES ('spirit','дух')")
#
#     for ru_w, en_w in zip(russian_words, english_words):
#         cursor.execute("INSERT INTO words (english, translate) VALUES ('{}','{}')".format(en_w, ru_w))
with sq.connect('technical dictionary.db') as con:
    cursor = con.cursor()
    cursor.execute("INSERT INTO words (english, translate) VALUES ('HELLO','WORLD')")
