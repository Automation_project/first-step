# Сочиняйте предложения используя выученные слова.
# Проверяет корректность написания слов на англ. И помогает заучить.
# В дальнейшем добавится функционал времён.

import random
import re

dictionary_for_Anki = open('dictionary for Anki.txt', 'r', encoding='utf-8')
dictionary_for_Anki = dictionary_for_Anki.read()
english_words = re.findall(r'(.*)' + r'\s' + r'[|]', dictionary_for_Anki, re.M)

russian_words = re.findall(r'[|]' + r'\s' + r'(.*)', dictionary_for_Anki)

tickets_dictionary = {}

for en_w, ru_w in zip(english_words, russian_words):
    tickets_dictionary[ru_w] = en_w

ru_w_keys = list(tickets_dictionary.keys())
random.shuffle(ru_w_keys)

wrong_eng = []

while True:
    if not ru_w_keys:
        break
    en_definition = []
    if len(ru_w_keys) >= 2:
        ru_definition = random.sample(ru_w_keys, 2)
        for each in ru_definition:  # Проверка на отсутствие дублирования новых определений и тех что сохр. В wrong_eng.
            if tickets_dictionary[each] in wrong_eng:
                ru_definition = random.sample(ru_w_keys, 2)

        print('\n'.join(ru_definition))
    else:
        ru_definition = random.choice(ru_w_keys)
        print(ru_definition)
        en_definition.append(tickets_dictionary[ru_definition])

    if wrong_eng:

        def_from_wrong_eng = random.choice(wrong_eng)  # добавляем случайное значение из w_e
        en_definition.append(def_from_wrong_eng)

        for k, v in tickets_dictionary.items():  # по значению выводим ключ
            if def_from_wrong_eng == v:
                print(k)

    if type(ru_definition) == list:  # Работает только для 2 или более ключей. Иначе добавлял буквы.
        for each_ru in ru_definition:
            en_definition.append(tickets_dictionary[each_ru])
    print('Create')
    user_essay = input()
    if user_essay == '':  # Если хотим дорешать все слова в w_e перед выходом.
        if wrong_eng:  # создаем список ключей для значений в w_e.
            temp_list = []
            for each in wrong_eng:
                for k, v in tickets_dictionary.items():
                    if each == v:
                        temp_list.append(k)
            ru_w_keys = temp_list  # заменяем, теперь весь цикл работает по этим ключам.
            wrong_eng = []

            continue
        else:
            break
    else:
        for each_en in en_definition:
            user_string = user_essay.lower().find(each_en)
            if user_string == -1:
                if len(ru_w_keys) > 20 and each_en not in wrong_eng:  # >20, что бы при дорешивании не добавлял ошибки
                    wrong_eng.append(each_en)
                print(each_en)
            else:
                if each_en in wrong_eng:
                    wrong_eng.remove(each_en)
                if len(ru_w_keys) < 20:
                    for k, v in tickets_dictionary.items():
                        if each_en == v:
                            ru_w_keys.remove(k)
print('End')