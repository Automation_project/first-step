import random
import re

dictionary_for_Anki = open('dictionary for Anki.txt', 'r', encoding='utf-8')
dictionary_for_Anki = dictionary_for_Anki.read()
english_words = re.findall(r'(.*)' + r'[|]', dictionary_for_Anki, re.M)

russian_words = re.findall(r'[|]' + r'(.*)', dictionary_for_Anki)

tickets_dictionary = {}
for en_w, ru_w in zip(english_words, russian_words):
    tickets_dictionary[en_w] = ru_w

en_w_keys = list(tickets_dictionary.keys())
random.shuffle(en_w_keys)

total_point = 0
n = 0
for tickets in range(5):
    for questionNum in range(3):
        correctAnswer = tickets_dictionary[en_w_keys[questionNum]]
        wrongAnswer = list(tickets_dictionary.values())
        del wrongAnswer[wrongAnswer.index(correctAnswer)]
        wrongAnswer = random.sample(wrongAnswer, 3)

        answer_option = wrongAnswer + [correctAnswer]
        random.shuffle(answer_option)
        print(en_w_keys[questionNum] + 'it\'s')

        print('Input right reply')
        user_answer = input()
        if user_answer == '':
            print('You want escape? Push enter if yes or no if you want continue.')
            escape = input()
            if escape == '':
                total_point = 0
                break
            else:
                continue
        else:
            words_in_user_answer = user_answer.split('. ')
            coincidence = 0
            for each in words_in_user_answer:
                if correctAnswer.find(each) > 0:
                    coincidence += 1
            if coincidence == len(correctAnswer.split(', ')):
                total_point += 10
            else:
                print(correctAnswer)

        print('')
    n += 1
    print('Your points : ', total_point)
    print('Round number ' + str(n))
    random.shuffle(en_w_keys)
    if total_point <= 10 * n:
        print('You lose')
        break
