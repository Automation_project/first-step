words | translate
--|--|**A**|
a lot of | �����, �����
a long on | ������, �������
abandon | �������, ������������
abilities | �����������
able | ���������, � ���������
acceptance | ��������
according | ��������
access | ������
account | ����(����������)
accurate | ������
accidentally | ��������
achieve | ���������
across | �����, ������
actions | ��������
addition | ����������
adieu | ������
adjective | ��������������
admit | ����������, ���������
adorably | �������������
advanced | ���������, �����������
advice | �����
advertisement | ��������� ����������
advance | �����������, ���������������
adverb | �������
afford | ���������, ������������
against | ������, ��
aged | � ��������
agenda | �������� ���
agree | �����������
ahead | �����, �����������
alert | �������
alerted | �����������
allows | ���������, ���������
allowed | ���������
almost | �����
along | �����, ������
although | ����, �������� ��
amateur | ��������, ������������
ambitious | �����������
ambiguous | �������������
amiss | �����������, �������, �����
amount | �����, ����������
anchor | �����
ancestor | ������
animal | ��������
ankle | �������
annoy | ����������
answer | �����, ��������
anticipate | �������, ����������
anxiety | ������������, �������
anymore | ������ (��), ��� (��)
apart | �� �����, ��������, ���� �� �����
applicable | ���������(�)
appalled | ��������
append | ���������, ������������
appropriate | ����������, �����������
appreciate | ������, ���������
appear | ����������, ��������
appearance | ���������, ������� ���
approve | ���������, ��������
appointment | ����������, �������
argue | ����������
arise | ���������, ����������
around | ������, �����
arrays | �������
arrange | ��������, ������������
aspect | ������
assume | ������������ (��� ���� ���������������)
assignment | ����������, ����������, �������
assemble | ��������
associate | ���������, ��������
attribute | �������, ��������
attach | ������������, ��������� (��������)
attract | ����������, ��������
attic | ������, ��������
attention | ��������
attempt | �������, ��������
attitude | ���������, �������
attrition | ���������
audition | �������������
audience | ���������
available | ������� � �������, ��������
avalanche | ������, �����
avoid | ��������
away | �����, ������
aware | �������������
awake | ����������
awful | �������
**B**|
backing | ���������
bar | ������, ������
barely | ����
barge | �����
basically | � ��������
batch | ������, ������, �����
beat | ����, ��������
behold | ������, ��������
being | v3, ����, �������� � ������ ������
below | ����
belong | ������������
benefits | ������������
beneath | ���, ����
besides | ������, ����� ����
betrayed | �������������
beyond | ���, ��
bid | ������, ������
bill | ������������, ����
binary form | � �������� �����
bind | ���������
biscuit | �������
blame | ����, ���������������, ���������, ��������
blast | �����, ����
bland | ������
bliss | ����������, �������
blink | �������
blow | ����, ��������, ���� 
blurry | ��������, ������
boil | �����, ��������, ������
bold | ������, �����������
border | ������� (country)
borrow | ��������, ������������
bottom-up | ����������, ����� ����, �������������
both | ���
bother | ����������
boundary | ������� (areas)
bounty | ��������, �������
boundless | ������������
bounce | ������������, ������
bozo | ������
brackets | ������
brave | �������, ������
brat | �������
breath | �������, ����
brew | ����������, ���������
brief | �������, ��������
bright | �����, �������
broad | �������, ����������
brook | �����
built in | ����������
bumbling | ���������
bummer | �����
bunch | ������, ������
burst | �������, ����������
butt | �������, ����
butter | ��������� �����
**C**|
cab | �����
callable | ����������
calm | ���������
cancer | ���
cape | ���, ����, �������
capacity | ���������, ��������
capable | ���������
capture | ������
carry | �����, ���������� 
care | ����, ������
carefully | �����������
carpet | ����
case | ������, �����
cause | �������, ����, �������
celebrate | �����������
certain | ������������,  ���������
chapter | �����
chase | ������, ������������
chandelier | ������
charm | ����������, �������
chairman | ������������
chatter | ��������, �����������, ��������
charity | �������������������
cheat | �����, �����������
cheesy | ������, �������
chest | ������� ������, ������
checkers | �����
chord | ������
circuit | �����, ����
citation | ������
clause | ��������, �����������, �����
clarify | ���������
clap | �������, ������������
claw | ������
clever | �����
climax | �����������
climb | ���������, ������
cloud | ������
clutch | ���������
cog | ������, �����
coincidence | ����������
column | �������, �������
complicated | �������, ����������
compile | ��������, ����������
completely | ���������, ����������
complain | ����������
compassion | ����������� 
comma | �������
common | �����
comeuppance | ���������
comprehension | ���������
command prompt | ��������� ������
comprise | ����������, ��������
comprehensive | ������������
compare | ����������, ���������
commitment | �������������
competition | ������������, �������
concern | ������������, �������.
contemptible | ����������
considerable | ������������, ������������
conveyance | ���������
convergence | ���������, �������
condition | �������, ���������, ���������
concludes | �������, ������ �����
conversation | ��������, ������
convoluted | ����������, ����������
conductor | ���������
consider | �������������, �������
confirm | ������������
convince | ��������
containment | �����������
contest | �������, ����������
confident | ���������������
cordial | ���������, ��������
count | �������, �������
couple | ����
counselor | ��������
court | ���
cover | ����������, ���������
crawl | �������
crack | �������
crave | �������
crew | �������, ������
creepy | �����
creature | ��������
crime | ������������
cross-legged | �������, �������� ����
crunch | �����
cursed | ���������, ��������������
cure | ���������, ������
current | �������, ���
currently | � ��������� �����
curtain | �������, �����
curious | ����������
custom | �������������
**D**|
dare | ������, �������
darn | �������
data types | ���� ������
declarations | ����������, ����������
decipher | ������������, ���������
decide | ������, ��������� �������
decimal | ����������
declare | ��������
decrease | ����������, ��������
decision | �������
decent | ���������, ���������
define | ����������
definition | �����������
defiant | �������, ����������
defy | ������� �����, ������������
degree | ������� (������)
degrees | �������
delude | ����������
delusion | �����������
demand | ���������, �����
deny | ��������
depot | �����, ���������, �������
depend | ��������
describe | ���������
deserve | �����������
descending | ����������, �����
desecrate | ����������
desperate | ���������
description | ��������
despite | �������� ��
desire | �������
despair | ��������
destiny | ������
determine | ����������
developer | �����������, ����������
develop | �����������, ���������
difficult | �������, ���������������
dignity | �����������, �����, ������
dinner | ����
dip | �������
directory | �������
dirt | �����
distract | �������, ���������
district | �����
distinct | ���������, ���������, ������
disappear | ��������, ���������
discover | ����������
disgusting | �������������, ������������
distant | ���������, ������
disease | �������, �����������
disguised | ���������������
discussion | ����������, ���������
disgrace | �����
divide | ���������
domain | �������, �����
doomed | �������
doom | ������
doubt | ����������� 
dozen | ������
drained | ���������
drop | �������, ������,  �����
due | �������
dump | �������, ������
dumb | �����, �����
during | � �������, �� �����
duty | ����, �����������
**E**|
earn | ������������, ��������
earthquake | �������������
earworm | ����������� �������
ease | ��������, ��������, ��������
edge | ����
effort | ������, �������
either | ���
elbow | �����, ��������������, ������� �������
elk | ����
elude | ����������
embarrassed | ���������
embrace | ���������, ��������
embark | ������, ������
emergency | ����������
employee | ��������, ���������
encourage | ��������, ���������
encore | ���
endless | �����������
engage | ������������, ����������
enhanced | �������, �������
enjoyed | ������������
enough | ����������
enormous | ��������, ���������
enroll | ����������, ��������������
enthuse | �������� ���������
entertainment | �����������
environment | ���������� ����������, �����
equipment | ������������
erase | �������
essential | �����������, ������, ��������
essentially | �� ����
essay | ���������
estate | ���������
establish | ����������, ��������
establishment | ����������
eventually | � ����� ������
even | ���� 
events | �������
ever wonder | ����� ������ �����������
examples | �������
exact | ������, ����������
exactly | �����, ������
exceptions | ����������
excited | �����������, ������������ � ��������
excuse | ����������, �������
execute | ���������, ���������, �������
exemption | ����������, ������������
execution | ����������
exhaustive | �������������
exist | ������������
explain | ���������
expect | �������, ������������
expressions | ���������
expensive | �������, �������������
explicitly | ����
experience | ����
explode | ����������
extension | ����������, ���������������
extensible | �����������
external | �������
**F**|
fade | ��������, �������
faith | ����
fairy | ���
failure | �������������, �������
familiar | ��������, ���������
fancy | ������, �����������, ��������
fares | ������
favor | ���������, ������
features | �����������, �������
feet | ����, �����, ����
feisty | ����������, ����������
fella | ������, ��������
fence | �����
feral | �����
festering | �������
fetch | ��������, ��������, ��������, 
few | �������, ����
field | ����
fierce | ��������, ��������
figure | ������, ����������
fine | � �������, �����
fired | ������
fit | ��������, �������������
flat | ��������, �������
flattery | �����
flame | �����, ������
flexible | ������, ��������� 
flow | �����, ����, �������
float | �������, ������, ��������� �����
flood | �����, ����������
flop | ������
flush | ���������, ������, �������
fluffy | ��������
fog | �����, ����
folks | ������� (������, ������������)
folder | �����
folk | �����
fool | �����
fortunate | ���������
forward | �����, ����������, ����������
former | ������
foreign | �����������, �������
forge | �������
forged | ����������
force | ����, �����, ���������
fraud | �������������, �����
frame | �����, ����, ����������
frequency | �������, �������������
frequent | ������
fright | �����, �����
frightening | ��������, ��������
from scratch | � ����
fruition | �����, �������������, �����������
funky | ���������
funeral | ��������
funding | ��������������
fur | ���, ������
futile | �����������
**G**|
garbage | �����
gear | ��������, ����������
gee | �� � ����
gently | �����, �����
genuine | ���������
general | �����
get | ��������, �����������
ghastly | �������
giant | ������, ����������
gist | ����
glad | ���������, ��������� 
glitter | �����, ��������
glorious | �������, ������������
glove | ��������
glow | ��������
glue | ����
gosh | ���� ������
grasp | ������, ��������
great | �������, ��������
grime | �����, ����
grievance | ������
ground | �����
grocery | �������
groceries | ��������
gruel | ����, ������
guardian | ������, ����������
guess | �����������, �������
guide | �����������
gum | ������
guts | �����, ��������
**H**|
habitual | ���������
hall | ���, �������, ����
handy | �������
hangover | ��������
handful | �������, ������
handsome | ��������
handle | ����������
happily | ���������
harm | ����, ���������
hazardous | �������, �����������
heavy | �������
heat | ����, ���������
header | ���������
heartfelt | ���������
heart | ������
headstone | ���������
heaven | ����, ���
headquarters | ����-��������
heck | �������
heighten | ��������
hellish | ������
hick | �����������, ���
high score | ������ 
highlight | ��������, ������������
highness | ����������
hijack | ������
hike | �����
hilarious | �������
hints | ���������
hire | ������, ���
hog | ������, �����
honestly | ������
hook(ed) | �������, ���� (����������)
hop | �������
horrify | �������
howl | ���
humiliate | �������
hurt | ������
hurtful | �������, ��������
hustle | ���������
hustler | ��������
**I**|
illumination | ���������
implement | �������������, ������������
impede | ��������������, ����������, ������
improve | ��������, �����������������
implementation | �������������, ����������
imperfection | ��������������, ������
impress | ����������
increase | ���������� (������)
include | ��������, ����������
increment | �������, ����������
incredible | �����������
inclusion | ���������, ����������
indefinite | ��������������
indentation | ������, ����������
independent | �����������
inevitably | ���������
influence | �������, ��������������
inhibition | ����������
initiative | ����������
injuries | ������
innards | ������������
insert | ���������, ������ ����������
insane | ��������, ��������������
instead | ������
insist | ����������
insight | ��������, ���������
instantly | ����������
instance | ������, ������
inspire | �����������
inscription | �������, ������
insurance | �����������
insured | ��������������
insecure | ������������
insipid | �������, ����������
insecurity | ������������
interactively | ������������
intolerable | �����������, �����������
intermediate | �������������, �������
intention | ���������
interrupt | ���������
invade | ����������
involve | �������� � ����, ���������������
invitation | �����������
investigation | �������������
irrelevant | ����������
irresponsible | ����������������
issues | �������
item | �������, �����
**J**|
jealous | ��������, �����������
jewelry | ���������, �������������
join | ��������������, ��������
jolly | ������, �����������
jumpsuit | ����������
just | ������, ������ ���
**K**|
kind | ������, ���, ���
kind of | ����� ���, ��� �����
kindle | �������
kindness | �������
knowledge | ������
knucklehead | ������
**L**|
laid out | ��������, �����������
lame | ���������, ������
lap | ����, ������
lately | � ��������� �����
lawn | �����, �������
least | ��������, ������ �����
leave | ���������, ��������, ������
ledge | ������
leftovers | �������
let | ���������, ����
liberty | �������
lie | ����, �����
likely | ��������, ��������
like | �������, ���
limit | ������, �����������
limo | �������
linking | ����������, ����������
list comprehension | ��������� ���������
little | ����, �������
literally | ���������, ��������
loan | ���
loose | ���������, �����������, �����������
look sharp | ������ � ��� 
look after | �������������, ������������
loosen | ��������
look | ��������
loud | ������, �����
low | ������, ���������
lunch | ����
**M**|
magic | �����
magician | ���������
magnificent | ������������
maid | ���������, ��������
malleable | ����������, ����������
malfunction | �������������
mallard | ����� ����
mammal | �������������
mandatory | ������������
mapping | �����������
mark | �������, ������, ������, ��������
marvelous | ��������
match | ������������, ����������, ����������
mate | ����, ��������
maverick | �������������
mayor | ���
meal | ���
measure | ����, ��������
mead | ��������
meaningless | �������������
mean | ��������
meantime | ��� ��������
melt | ��������
mention | ���������
merge | ����������, ���������
merry | �������
mercy | ����������
mess | ����������
midair | � �������
mindfulness | ���������������
mind | ��, �����
miraculously | �������� �������
mirth | �������, �������
misery | ���������, ������, ���������
moldy | �������������
monk | �����
mood | ����������
moose | ����
mope | ��������
moron | �����
mount | �������������, ����������
moving around files | ����������� ������
mow | ������
mucus | �����
mud | ����� (���������)
multiple | �����, ������������, �������������
mundane | �����������, ���������
mutual | ��������, �����
muzzle | �����, ���������
myself | � ���
**N**|
naive | �������
nail | ������, �������
nasty | ����������, ���������
nearby | �����, ����������
neat | ����������
necessary | ����������
negligence | �����������, ����������
neither | �� ����, ��
nervous | �������
nested | ���������
networking | ����
newcomer | �������
notice | �����������, ���������
noticeable | ��������
notable | ��������, ��������������
note | �������, ����������, ��������
noun | ���������������
numeric | ��������, ��������
**O**|
obstinate | �������
obsolete | ����������
obsess | �������������
observation | ����������
obsession | �����������
obvious | ���������
occasion | �����, �������
occur | ����������
odd | ��������
offensive | �����������, ��������������
offend | �������, ���������
once | �������
one | ����, �������, ������
onto | ��, � (�����������)
ooze | ��, �����, ��������
opportunity | �����������
option | �������, �����������
ordered (by) | (��) �������, ������
order | �������, ������, �����
ordinary | �������
otherwise | �����, � ��������� ������
otter | �����
ought | ������
outfit | �����, ������, ������������
output | ���������, �����
out | ��, ���, ���
ovarian | ������ (�������.������)
over a large | ��� �������
overwhelm | ��������, ���������, ���������
overview | �����
oversight | ����������, ������
overdue | ������������
overreach | �����������
**P**|
pageant | ���, �������
palace | ������
pal | ��������
particular | ������, ����������, �������������
past | �������, ����
pattern | ������, ������
patience | ��������
path | �������, ����
pathetic | ������
patient | �������, ����������
payment | ������, �����
pencil | ��������
perform | ���������, ���������
perhaps | ��������
per diem | ��������
perfectly | �������, ����������
perilous | �������
performance | ������������������
pesky | �����������
piece | �����, �����
pinky | �������
pine | �����
placeholder | �����������, ����������
plate | �������, ��������, �����
pleased | ���������
pleasure | ������������
pointless | �������������
pointy | �����������
ponies | ����
pop up | ���������, �������
porky | ������
possible | ���������(��)
pot | ������, ��������
pout | ������, �������
practice | ��������, ����������
pray | ��������
pretty pissed | �������� �����������
pressure | ��������
presence | �����������, �������
prepare | �����������
precisely | ������, �����
predict | �������������
pretend | ������������
prey | ������, ������
pregnant | ����������
pretty | ��������, ��������
preposition | �������
pristine | ����������, ������ 
primary | ���������, �������
private | �������
provide | ������������, �������������
probably | ��������, ��������
property | ��������, ���������, �������������
pronounce | �����������, ���������
produce | �����������, ���������
properly | ���������, ������� �������
pronoun | �����������
puke | �������, �����
punishment | ��������� 
purse | �������, �������
pursuit | �������������
purpose | ����, ������
pursue | ����������, ������������
put | ������, �������
**Q**|
quarrel | �����
query | ������, ������
queue | �������
quiet | �����, ���������
quite | ��������, ������
quiz | ���������, �����������
quirky | �����������
**R**|
rabies | ���������
races | ������, ���
rack | ������
raise | ��������� (object)
rate | ����������, ����, ������
rather | ��������, ������
razor | ������, �����
rearrange | �����������, ��������
reached | ���������
realize | ��������, ����������
really | �������������, �����
rebuild | ���������������
rebel | ���������, �������������
recognize | ������, ��������
recently | �������, � ��������� �����
receive | ��������
recent | ��������, ���������
recall | �������, ���������, �����
recycling | �����������
reckoning | ��������, ������
reckon | �������, ��������
recluse | ���������, ���������
reckless | ������������
reduce | ���������, ���������
refuse | ��������, ����������
reflect | ��������
reference | ������, �������
refurbish | �������������
regret | ��������, ������������
regex | ���������� ���������
rehearsal | ���������
reign | �����������
rejuvenation | ����������, �������������� ���
relatively | ������������
rely | ����������, ���������
remove | �������, �������
reminisce | ����������
reply | ��������, �����
repossess | ������� �� �����, ���������� �� ��������
repeat | ���������, ����������, ���������
replacement | ������
repetitive | ����������
repeatedly | ������������
require | ���������
restlessness | ������������ (res(t)l?sn?s)
rest | �����, �������
responsibility | ���������������, �����������
rescue | ��������
respond | �������� (� �����)
responsible | �������������
resignation | ��������
restrain | ����������
resist | ��������������
retain | ����������, ���������
reunite | ��������������
reveal | ��������
revert | ���������
rhombus | ����
ridiculous | �������
rigorous | ����������, �������
rise | �����������, ����� (subject)
rival | ��������, ���������
rodent | ������
round | �����, ����, ������
router | �������������
rough | ������
rug | ����
rumors | �����
rusty | ������
**S**|
safety | ������������
sagging | ����������, ������
salary | ��������, �����
same | ����������, ��� ��
sample | ������, �������
sans | ���
savage | �����
scary | ������
scenic | ����������
schedule | ����������, ������
scissors | �������
scope | �����, �����, �����������
script | ��������
scratch | ��������
screwdriver | �������
scram | �����������
seem | ��������, ���������
select | ��������, ��������
sentence | �����������, �����������
sense | �����, �������, ��������
separate | ���������, ���������
sequence | ������������������
settle | �������������, �������
several | ���������
sewer | �����������
shape | �����
sharp | ������, ������
shame | ����, �����
shake | ������, ������
shell | ��������
shelve | ����������� � ������ ����
shelf | �����
shine | �����, �����
shut | �������, ��������
shuffle | ������������, ��������, �������
shush | �����������
shy | �����������
sick | �������, �������
side | �������, ���
sign | ����, ���������
silly | �����, �������, ����������
sink | ��������, ��������
sitter | ����
skedaddle | ����������
skirt | ����, �����, ����
skier | ������ 
skip | ����������
skulk | ���������
sledge | ����, �����
slipping | ���������
sly | ������
smooth | �������, ������
snatch | ��������, ������
snare | �������
sneak | ��������
snot | �����
software | ����������� �����������
solve | ������
soon | �����, ����
soulful | ��������
span | ������, ��������
spare | ��������, ���������
spark | �����
spending | �������, �����
spelling | ������������, ����������
spectacular | ������������, �������������
species | ���
spike | ���, ���
spirit | ���
spot | �����, �����
spoil | �������
sprinkle | ��������, ����������
spread | ���������������
squid | �������
squalor | ������
stake | ������, ����, ���
stag party | ����������� ��������� 
statement | ���������, �����������, ����������
stand | ������, �������
stamina | ������������
stagehand | ������� �����
state | ���������, �����������
stairs | ��������
stage | �����, ����
stem | ������, ��������, ���
steam | ���, ��������
steep | ������, ������
still | �� ��������, �� ���
stick | �����, ��������������
stitch | ����, ������ 
stored | ��������
stomach | �������
straight | �����, �����, ����������
strike | ����, ����������
strange | ����������, ��������
streamline | ��������������, �����������
struggle | ������
stuff | ����
stubbornness | ���������
stuffed | �������������
such | �����, ������������
successfully | �������
sudden | ���������, �����������
suffocate | ������, ���������� 
suffer | ��������
suggest | ����������
suite | �����, ��������
sunset | �����
supposed | ��������������
suppose | ������������, ���������
supervisor | ������������
surface | �����������, �������������
surround | ��������, ��������
sustenance | �������� � �������������, ���������
suspect | �������������, �����������
swear | ��������
sweaty | ������
switch | �������������
**T**|
tailor | �������
tail | �����
tall | �������, �������
talent | ������
tale | ������
tame | ���������, ��������
task | ������
tattling | ��������, �����������
tear | ���������, �����
tedious | ������������, ������
teensy | ���������
temporary | ���������
template | ������
tense | �����������, �����
tenderly | �����
terminate | ����������
terrific | �����������
than | ���
thankless | �������������
there | ���, �����
themselves | ��� ����, ��������������
thirsty | �����, ���� ����
thing | ����, �������
think | ������, �����
thick | ������, �������
those | ���, ����
though | ����
throw-away programs | ����������� ���������
throughout | � �������, �� ����������
throw | �������
thug | ������
tickle | ��������
tightly | ������, �����, ������
till | ����, ��
tire | ����
tiredness | ���������
toes | ������ ���
togetherness | ��������
token | ����, ������
tongue | ����
totally | ���������
tough | �������, �������
tout | �������������
traffic | ��������, �������
tram | �������
traitor | ���������
trent | �������, �����
trespass | ��������������
treat | ������
trick | ����, ������
tribe | �����, ����
trip | �������, �����������
trim | �������
trousers | ����� 
trout | ������
truthful | ���������
trunk | �����
trying out | ���������
tucked | ����������, ���������
tune | ���������
tuple | ������
tuxedo | �������
twice | ������, �����
type | ���, ������, ����������
**U**|
unacceptable | �����������(��)
unaffected | ����������, ��� ���������
uncle | ����
unemployment | �����������
unite | ����������
unobstructed | �����������������
unrelated | �����������
unsettle | ������������, �������� �� �����
unsettled | ����������, �����������������
unwind | ������������, ��������
upon | ��, ��, �, �� (������� ��������)
upset | ����������, ������������
usage | �������������, ����������
usher | �������, �������
**V**|
vacation | ������
vain | ����������, �������
valid | ��������������, �����������
van | ������
variable | ����������
vehicle | ������������ ��������, ����������
velvet | ������
vendor | ���������
vengeance | �����
verb | ������
vest | �����
viable | ��������������
vibe | ���������
vigilant | ����������
vile | ������, �������, ��������������
violence | �������, ���������, ����
violation | ���������
visionary | ��������
void | �������
vowel | ������� ����
**W**|
wager | ����, ������
wander | �������, ��������
warm | ������, �����������
warn | �������������
wardrobe | ��������
warrant | �����
waste | ������, �����
watcher | �����������
wearing | ������, ������������
wealth | ���������
weak | ������
wear | ������, ��������
weasel | �����, �������
wee | ���������, ���������
weirdness | ����������
weight | ���, �����
weirdo | �����
well-being | ������������, ��������������
well-suited | ������ ��������
welfare | ��������������
whatever | ��� �� ��, �����
wherever | ��� �� ��, �����
which | �������, �����
whittle | ��������
while | � �� ����� ���, ����
whole | ����, �����
widths | ������
wide | �������, �������
wisdom | ��������
withdraw | �������, ��������
witness | ���������, �����������������
witty | ����������
womb | �����, �����, ����
wonder | ����������, ����
wool | ������
worth | ��������
workshop | ����������
worry | �����������, ������������
wretched | �������, ������, ����������
**X**|
x-rays | ������������� ����
**Y**|
yard | ����, ���, ��������
yell | ������, �������
yet | ���, ��� �� �����, ����� ����
**Z**|
zeal | ������
