import random

# wrong_dict = {'backpack': ['tea', 'frog'], 'bag': ['pen', 'clock'], 'tea': ['ice', 'due'], 'frog': ['ought', 'hog']}
#
# country, capital = random.choice(list(wrong_dict.items()))
# print(country, random.choice(capital))
#
# print(sec_capital := random.choice(list(wrong_dict.items())))

tense_dict = {
    'Pr. Continuous': ['Указание на процесс, происходящий непосредственно в момент разговора',
                       'Действие, находящееся в процессе своего развития',
                       'Процесс начался ранее и не закончится, он является длительным.',
                       'Get, change, become, increase, rise, fall, grow, improve, begin, start.'
                       ]


}

tense, case = random.choice(list(tense_dict.items()))

print(tense, '\n', random.choice(case))