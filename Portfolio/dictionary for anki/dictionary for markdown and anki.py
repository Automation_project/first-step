# В буфере обмена новая строка с англо-русскими словами.
# Перевод от англ. слова отделён двойным пробелом или точка, пробел.
# Эти данные проверяются на совпадение в словаре. Затем интегрируются в него.
# Словарь определяется в алфавитном порядке до первых 3 букв в слове.
# Создаются еще два словаря адаптированных под программу 'Anki droid'
#
# Добавить:
# Не работает с глаголами to verb, потому что по 3 символ пробел.
# Проверку наличия новых слов в итоговом списке.
# Убирать лишние пробелы перед словами. А так же в конце и в начале списка новых слов.

import pyperclip
import re
import string

technical_dictionary = open('technical dictionary.txt', 'r+', encoding='windows-1251')
dictionary = technical_dictionary.read().lower()[26:] + '\n'
# \n добавлено, потому что при присоединении новых слов
# первый элемент слипается с последним в таблице.


new_words = re.sub(r'  |\. ', ' | ', pyperclip.paste().replace('\r', '')).lower()  # НЕ МЕНЯТЬ НА \s\s
# replace, потому что на windows символ новый строки \r\n.
words_in_new_words = re.findall(r'^.*', new_words, re.M)

corrected_words = []
for each_new_words in words_in_new_words:
    only_en_w = re.search(r'(^.*)\|', each_new_words, re.M)
    totally_have_match = re.search((r'^' + only_en_w.group(1)) + r'\|(.*)', dictionary, re.M)

    if totally_have_match:  # Если совпадение по англ. слову найдено.
        have_match = re.search(r'^' + only_en_w.group(1), dictionary, re.M)
        print('Found a coincidence:', each_new_words)
        example = re.search(r'^' + have_match.group() + r'\|(.*)', dictionary, re.M)
        print(example.group())
        print('Enter new data if necessary, or press Enter to continue. ')
        match new_definitions := input():  # Позволяет прибавить новое определение, в случае совпадения.
            case '':
                print('skip')
                continue
            case _:
                change_example = example.group(1) + ', ' + new_definitions
                dictionary = re.sub(example.group(1), change_example, dictionary)
                corrected_words.append(change_example)
    else:
        dictionary += each_new_words + '\n'
        print(each_new_words)
print('Please wait a few minutes.')
alphabetical_dictionary = []

for thirst_letter in string.ascii_letters:
    huge_title_letters = ('**' + thirst_letter.upper() + '**' + '|')
    alphabetical_dictionary.append(huge_title_letters)

    empty_second_letter = re.findall(r'^' + thirst_letter + r'\W' + r'.*', dictionary, re.M)
    if empty_second_letter:
        for match_e_s_l in empty_second_letter:
            alphabetical_dictionary.append(match_e_s_l)

    for seconds_letter in string.ascii_letters:
        for third_letter in string.ascii_lowercase:
            third_letter = re.findall(r'^' + thirst_letter + seconds_letter + third_letter + r'.*', dictionary, re.M)
            if third_letter:
                for match_third_let in third_letter:
                    alphabetical_dictionary.append(match_third_let)
    if thirst_letter == 'z':  # Отменяет повторную итерацию с добавлением **А**| и т.д
        break

final_tech_dict = ''
for each_words in alphabetical_dictionary:
    final_tech_dict += each_words + '\n'

technical_dictionary.seek(26)
technical_dictionary.write(final_tech_dict)
pyperclip.copy('words | translate\n' + '--|--|\n' + final_tech_dict)  # Откуда-то добавляет в начале \n ????

dictionary_for_Anki = re.sub(r"(\W\W\D\W\W\W)", '', final_tech_dict)
my_file = open('dictionary for Anki.txt', 'w+', encoding='utf-8')
my_file.write(dictionary_for_Anki)

russian_words = re.findall(r'[|]' + r'.*', dictionary_for_Anki)
english_words = re.findall(r'^' + r'.*' + r'[|]', dictionary_for_Anki, re.M)

inverse_dictionary = ''
for ru_w, en_w in zip(russian_words, english_words):
    new_line = '\n{} | {}'.format(ru_w[2:], en_w[:-2])
    inverse_dictionary += new_line

change_inverse_dictionary = open('inverse dictionary.txt', 'w+', encoding='utf-8')
change_inverse_dictionary.write(inverse_dictionary)

if corrected_words:
    print('Corrected words:')
    for each in corrected_words:
        print(each)
