# Проводит поиск нужного значения среди любого предоставленного текстового файла находящихся в директории.
# Находит слова по последовательности букв.
import re

inverse_dictionary = open('inverse dictionary.txt', 'r', encoding='utf-8')

x = inverse_dictionary.read()
while True:
    print('Input your word')
    user_word = input()
    if user_word == '':
        break
    else:
        # Нижеприведенный код позволял найти конкретные слова.
        # Текущий находит все совпадения.
        # if having_match := re.findall(r'^' + user_word + r'\W.*', x, re.M):
        #     for coincidence in having_match:
        #         print(coincidence)
        # print('-----')
        # if having_match := re.findall(r'.* ' + user_word + r'\W.*', x, re.M):
        #     for coincidence in having_match:
        #         print(coincidence)
        # print('-----')
        if having_match := re.findall(r'.*' + user_word + r'.*', x, re.M):
            for coincidence in having_match:
                print(coincidence)
