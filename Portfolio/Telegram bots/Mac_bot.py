import telebot
from telebot.types import InlineKeyboardMarkup, InlineKeyboardButton

bot = telebot.TeleBot("6387549874:AAG1tnRJ7IBg6e0CUqPERB0iPfJz4PUQVrE")

# Кнопки с названиями популярных позиций в McDonald's
menu = InlineKeyboardMarkup(row_width=2)
menu.add(InlineKeyboardButton("БигМак", callback_data='bigmac'),
         InlineKeyboardButton("Чизбургер", callback_data='cheeseburger'),
         InlineKeyboardButton("Картофель Фри", callback_data='fries'),
         InlineKeyboardButton("Кока-Кола", callback_data='coke'),
         InlineKeyboardButton("МакФлурри", callback_data='mcflurry'),
         InlineKeyboardButton("Наггетсы", callback_data='nuggets'),
         InlineKeyboardButton("Корзина", callback_data='cart'))

# Словарь с ценами на товары
prices = {'bigmac': 200, 'cheeseburger': 100, 'fries': 80,
          'coke': 50, 'mcflurry': 150, 'nuggets': 120}

# Словарь со ссылками на изображения товаров
image_urls = {'bigmac': 'https://drive.google.com/uc?id=1lf8vp2-dAJKwZanwhiSueB96YDTb8BIE',
              'cheeseburger': 'https://drive.google.com/uc?id=1g2nry1-YzldHoGRFpH2Ubdzla6NpbMl7',
              'fries': 'https://drive.google.com/uc?id=1Yc0UICdUhTkq3KSTQR5K27HYBjxZMmEW',
              'coke': 'https://drive.google.com/uc?id=1PQnFncxVKkOkIB_kl2WA58WelJ9o_jUW',
              'mcflurry': 'https://drive.google.com/uc?id=1npXBC9-GdsqLhjmgdAv8LoZ8wAnX3npY',
              'nuggets': 'https://drive.google.com/uc?id=1DGBDNSAtJJ0lbO5Mq9LHaIRsHi3a_OTi'}


# Корзина пользователя
cart = {}

# Обработчик нажатий на кнопки
@bot.callback_query_handler(func=lambda call: True)
def callback_handler(call):
    if call.data in prices:
        # Добавляем товар в корзину
        if call.data in cart:
            cart[call.data] += prices[call.data]
        else:
            cart[call.data] = prices[call.data]
        # Отправляем изображение товара и цену
        bot.send_photo(call.message.chat.id, image_urls[call.data],
                       caption=f'<b>Цена: {prices[call.data]} руб.</b>',
                         reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton('+1 к заказу',
                                                                                  callback_data=f'add_to_cart_{call.data}')]]), parse_mode='HTML')
        # Добавляем кнопки меню, поскольку изображения занимают большую часть чата.
        bot.send_message(call.message.chat.id, '<b>Выберите товар из меню:</b>',
                         reply_markup=menu, parse_mode='HTML')
    elif call.data == 'cart':
        # Отправляем содержимое корзины
        if cart:
            total = sum(cart.values())
            items = '\n'.join([f'<b>{item.capitalize()}:</b> {cart[item] // prices[item]} шт. ({cart[item]} руб.)' for item in cart])
            bot.send_message(call.message.chat.id, f'<b>Ваша корзина:</b>\n{items}\n<b>Итого:</b> {total} руб.',
                             reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton('Очистить корзину', callback_data='clear_cart')],
                                                                [InlineKeyboardButton('Купить', callback_data='buy')]]), parse_mode='HTML')
        else:
            bot.send_message(call.message.chat.id, f'<b>Ваша корзина пуста</b>', parse_mode='HTML')
    elif call.data.startswith('add_to_cart_'):
        item = call.data.split('_')[-1]
        if item in cart:
            cart[item] += prices[item]
        else:
            cart[item] = prices[item]
        bot.send_message(call.message.chat.id, f'{item} добавлен в корзину', parse_mode='HTML')
    elif call.data == 'clear_cart':
        # Очищаем корзину
        cart.clear()
        bot.send_message(call.message.chat.id, f'<b>Ваша корзина очищена</b>', parse_mode='HTML')
        # Добавляем кнопку "Начать заново"
        bot.send_message(call.message.chat.id, '<b>Добро пожаловать в McDonald’s! Выберите товар из меню:</b>',
                         reply_markup=menu, parse_mode='HTML')

    elif call.data == 'buy':
        # Отправляем сообщение о том, что функционал находится в разработке
        bot.send_message(call.message.chat.id, 'Данный функционал находится в разработке.',
                         reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton('Очистить корзину и начать заново', callback_data='clear_cart')]]),
                         parse_mode='HTML')

# Обработчик команды /start
@bot.message_handler(commands=['start'])
def start_handler(message):
    bot.send_message(message.chat.id, '<b>Добро пожаловать в McDonald’s! Выберите товар из меню:</b>',
                     reply_markup=menu, parse_mode='HTML')

# Обработчик команды /cart
@bot.message_handler(commands=['cart'])
def cart_handler(message):
    if cart:
        total = sum(cart.values())
        items = '\n'.join([f'<b>{item.capitalize()}:</b> {cart[item] // prices[item]} шт. ({cart[item]} руб.)' for item in cart])
        bot.send_message(message.chat.id, f'<b>Ваша корзина:</b>\n{items}\n<b>Итого:</b> {total} руб.',
                         reply_markup=InlineKeyboardMarkup([[InlineKeyboardButton('Очистить корзину', callback_data='clear_cart')]]),
                         parse_mode='HTML')
    else:
        bot.send_message(message.chat.id, '<b>Ваша корзина пуста</b>', parse_mode='HTML')



bot.polling()
