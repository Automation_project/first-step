import pyperclip
import re

wrong_title = pyperclip.paste()

dictionary = ['Kia', "Bwm", 'Audi']

title = ''

for each in dictionary:
    if wrong_title.find(each) >= 1:
        title = re.sub(each, each.upper(), wrong_title)
        pyperclip.copy(title)

