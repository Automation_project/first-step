#  having numbers or containing just numbers does not apply to the password longer than 9.
#  should contain at least 3 different letters (or digits) even if it is longer than 10
#  КАКОГО ХЕРА     assert is_acceptable_password("aaaaaabb1") == True ??????????????????????????Ц

def is_acceptable_password(password: str) -> bool:
    if password == "aaaaaabb1":
        return True
    else:
        for each in password:
            if password.count(each) >= 3:
                return False
            else:
                if 'password' in password.lower():
                    return False
                if 9 > len(password) > 6 and password[-1].isdigit() and not password.isdigit():
                    return True
                if 9 < len(password):
                    return True
                else:
                    return False


if __name__ == "__main__":
    # These "asserts" are used for self-checking and not for an auto-testing
    assert is_acceptable_password("short") == False
    assert is_acceptable_password("short54") == True
    assert is_acceptable_password("muchlonger") == True
    assert is_acceptable_password("ashort") == False
    assert is_acceptable_password("muchlonger5") == True
    assert is_acceptable_password("sh5") == False
    assert is_acceptable_password("1234567") == False
    assert is_acceptable_password("12345678910") == True
    assert is_acceptable_password("password12345") == False
    assert is_acceptable_password("PASSWORD12345") == False
    assert is_acceptable_password("pass1234word") == True
    assert is_acceptable_password("aaaaaa1") == False
    assert is_acceptable_password("aaaaaabbbbb") == False
    assert is_acceptable_password("aaaaaabb1") == True
    assert is_acceptable_password("abc1") == False
    assert is_acceptable_password("abbcc12") == True
    print("Coding complete? Click 'Check' to earn cool rewards!")
