#  В строке два или более предоставленных значений. Найти индекс второго вхождения.
#  find строка (срез от find один до конца). Если нет, то None.

text = 'hi mayor'

symbol = ' '

x = text.find(symbol) + 1
y = text[x:].find(symbol)
if y == -1:
    print('None')
else:
    print(y+x)