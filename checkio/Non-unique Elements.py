#  Дан список. Удалить из него неуникальные элементы.
#  Вывести список, сохранив порядок неуникальных элементов.
#  Лучшее решение: return [x for x in data if data.count(x)>1]

some_list = [10, 9, 10, 10, 9, 8]
data = []
for each in some_list:
    if some_list.count(each) >= 2:
        data.append(each)

print(data)