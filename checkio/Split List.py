# shame

def split_list(items: list) -> list:
    if not items:
        return [[], []]

    length = len(items)
    first = []
    second = []
    n = 0

    if length == 1:
        return [[items[0]], []]

    elif length % 2 == 0:
        while True:
            if n < length / 2:
                first.append(items[n])
                n = n + 1
            elif length / 2 <= n < length:
                n = n + 1
                second.append(items[n - 1])
            else:
                return [first, second]
    else:
        while True:
            if n < length / 2:
                first.append(items[n])
                n = n + 1
            elif length / 2 <= n < length:
                n = n + 1
                second.append(items[n - 1])
            else:
                return [first, second]


if __name__ == '__main__':
    assert split_list([1]) == [[1], []]
    assert split_list([]) == [[], []]
    print("Example:")
    print(split_list([1, 2, 3, 4, 5, 6]))

    # These "asserts" are used for self-checking and not for an auto-testing
    assert split_list([1, 2, 3, 4, 5, 6]) == [[1, 2, 3], [4, 5, 6]]
    assert split_list([1, 2, 3]) == [[1, 2], [3]]
    assert split_list([1, 2, 3, 4, 5]) == [[1, 2, 3], [4, 5]]
    assert split_list([1]) == [[1], []]
    assert split_list([]) == [[], []]
    print("Coding complete? Click 'Check' to earn cool rewards!")
