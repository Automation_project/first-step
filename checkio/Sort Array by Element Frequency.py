#  Right result:[4,4,4,4,2,2,2,6,6]
# sorted(items, key=lambda x: (-items.count(x), items.index(x))) - чистое решение.

items = [4, 6, 2, 2, 2, 6, 4, 4, 4]

unique_elements = []
for elements in items:  # Отбор уникальных значений
    if elements not in unique_elements:
        unique_elements.append(elements)


count_dict = {}
for each_elem in unique_elements:  # Подсчет количества ун. элементов, компоновка в словарь.
    count_dict[each_elem] = items.count(each_elem)

sorted_keys = sorted(count_dict, key=count_dict.get, reverse=True)  # список ключей, отсортированных по значениям.


sorted_dict = {}
for w in sorted_keys:  # Создание словаря, где к ключам ранее отсортированным по значениям
    sorted_dict[w] = count_dict[w]  # привязываются сами значения.

output = []
for key, val in sorted_dict.items():  # Теперь создаём список, в который добавляем элементы списка items
    k = 0
    while k != val:  # В количестве, равном ранее подсчитанных повторений.
        output.append(key)
        k += 1

print(output)
