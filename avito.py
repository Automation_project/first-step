import pyperclip

title = pyperclip.paste()

short_title = ''

for each in title.split()[:8]:
    short_title += each + ' '

pyperclip.copy(short_title[:-1])


